# Description

A basic multi-purpose template.

# Dev Notes

## Building Installation Files

* To package the extension into an installation file execute 
`/dev/joomla-extension-builder.sh` 

* The installer will be placed in the `dev/bin` folder if it exists. 

* Otherwise it will be placed in your home folder.

## Minifying JS and CSS

* Run `npm install` to install the necessary modules for the minification process.
 
* To minify files, navigate to the `dev/grunt` folder in the terminal and run the `grunt` command to minify all files specified in the GruntFile.js

* If you need to minify new files, add them to `/dev/grunt/Gruntfile.js`
