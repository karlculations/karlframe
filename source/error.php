<?php
/**
 * @version		$Id: error.php 17282 2010-05-26 15:24:49Z infograf768 $
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php 
	$templateparams	=  JFactory::getApplication()->getTemplate(true)->params;
	$menuTitle = $templateparams->get('menutitle','Main Menu');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php $doc=JFactory::getDocument(); $language=$doc->language; ?>
<meta name="language" content="<?php echo $language; ?>" />
<title><?php echo $this->title; ?></title>
<style type="text/css">
	body { background:#f9f9f9; }
	.error { text-align:center; margin-top:15px; }
	#outline { background:white; border:1px solid #666; border-radius:5px; width:500px; margin:0 auto; }
	#errorboxheader { background:#666; color:white; font-weight:bold; text-align:center; padding:3px; }
	#errorboxbody { margin:10px; text-align:left; }
	#techinfo { background:#eee; border:1px solid #ddd; padding:5px; }
	#techinfo p { margin:5px; }
	a { color:#03c; text-decoration:none; }
	li.current { font-weight:bold; }
	ul ul { display:none; }
</style>
</head>
<body>
	<div class="error">
		<div id="outline">
			<div id="errorboxoutline">
				<div id="errorboxheader"> <?php echo $this->error->getMessage(); ?></div>
				<div id="errorboxbody">
					<?php
					if ($this->error->getCode()>=400 && $this->error->getCode() < 500) {
						echo '<p><strong>The page you requested could not be found.</strong></p>';
						
							if (@JModuleHelper::getModule( 'menu' )) {
								echo '<p><strong>' . JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES') . '</strong></p>';
								$module = JModuleHelper::getModule( 'menu', $menuTitle);
								echo JModuleHelper::renderModule( $module);
							}
						
							if (@JModuleHelper::getModule( 'search' )) {
								echo '<p><strong>You can also search the site:</strong></p>';
								$module = JModuleHelper::getModule( 'search' );
								echo JModuleHelper::renderModule( $module);
							}
					} else {
						
						echo '<p><strong>An error has occurred.</strong></p>';
						echo '<p><strong>' . JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES') . '</strong></p>';
						echo '<ul><li><a href="' . $this->baseurl . '/" title="' . JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE') . '">' . JText::_('JERROR_LAYOUT_HOME_PAGE') . '</a></li></ul>';
						
					}
					?>
					
					<p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></strong></p>
					<div id="techinfo">
						<p><?php echo 'Error ' . $this->error->getCode() . ' - ' . $this->error->getMessage(); ?></p>
						<p>
							<?php if ($this->debug) :
								echo $this->renderBacktrace();
							endif; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
