<?php

// No direct access.
defined('_JEXEC') or die;
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

require_once 'KarlFrameBootstrap.php';

# Include and configure libraries and resources (scripts/stylesheets).
require_once 'KarlFrameResource.php';
require_once 'KarlFrameLibraries.php';
KarlFrameLibraries::$templatePath = $this->template;
KarlFrameLibraries::$baseUrl = $this->baseurl;

JHTML::_('behavior.framework', true);

// Hide the generator so hackers can't easily see our Joomla version.
$this->setGenerator('');

// Get a reference to the current document. This will be useful later.
$document = JFactory::getDocument();

// Determine if we're on the home page (the active menu item will be the default menu item)
$menu = JFactory::getApplication()->getMenu();
$frontPage = ($menu->getActive() == $menu->getDefault());

// Determine domain (currently only used for Analytics)
$domain = preg_replace('/^www\.|[^a-zA-Z0-9._-]/', '', $_SERVER['SERVER_NAME'] ?: $_SERVER['HTTP_HOST']);

// This will store key/values for KarlFrame's javascript object.
$js = [];

// Determine values for text variables
$pageTitle = $this->params->get('page-title');
$homePageTitle = $this->params->get('home-page-title');
$analyticsMode = $this->params->get('ga-mode', 'universal');
$analyticsID = $this->params->get('analytics-id');
$gtmId = $this->params->get('gtm-id');
$gtmOpt = $this->params->get('gtm-opt');
$analyticsCrossDomain = $this->params->get('analytics-cross-domain');
$otherCSS = $this->params->get('other-css');
$otherLESS = $this->params->get('other-less');
$otherJS = $this->params->get('other-javascript');
$ie6AlphaSelectors = $this->params->get('ie6-alpha-selectors');
$ie6StaticAlphaSelectors = $this->params->get('ie6-static-alpha-selectors');
$css3pieSelectors = $this->params->get('css3-pie-selectors');
$googleFonts = $this->params->get('google-fonts');
$scripts = $this->params->get('scripts');
$faviconCode = $this->params->get('favicon-code');
$socialProfiles = $this->params->get('social-profiles');
$logo = $this->params->get('logo');
//$jquery = $this->params->get('jquery');
$jqueryColor = $this->params->get('jquery-color');
$jqueryUI = $this->params->get('jquery-ui');
$jqueryUItheme = $this->params->get('jquery-ui-theme');
$less = $this->params->get('less');
$capJs = $this->params->get('captionjs');
$coreJs = $this->params->get('corejs');
$cacheVersion = $this->params->get('cache-version');
$metaViewport = $this->params->get('meta-viewport');
$appleIconType = $this->params->get('apple-icon-type');
$windowsTileColour = $this->params->get('windows-tile-colour');
$countryWhitelist = $this->params->get('country-whitelist');
$respondjs = $this->params->get('respondjs');
$scriptType = 'text/javascript';
$visitCookieExpiry = $this->params->get('visit-cookie-expiry');
$visitCookieSuffix = $this->params->get('visit-cookie-suffix');
$carousel = $this->params->get('carousel');
$slickCarouselVersion = $this->params->get('slick-carousel-version');
$cookieData = $this->params->get('cookie-data');

// Determine boolean values for checkbox variables
// The variation between 'no' and 'yes' is to better support the change from checkboxes to radio buttons.
$menuCSS = $this->params->get('menu-css') !== 'no';
$customCSS = $this->params->get('custom-css') !== 'no';
$karlframeJS = $this->params->get('karlframe-js') !== 'no';
$jqueryIdTabs = $this->params->get('jquery-idtabs') !== 'no';
$responsive = $this->params->get('responsive') === 'yes';
$geolocation = $this->params->get('geolocation') === 'yes';
$ie6Fixes = $this->params->get('ie6-fixes') !== 'no';
$ieCSS = $this->params->get('ie-css') !== 'no';
$css3pie = $this->params->get('css3-pie') !== 'no';
$jqueryNoConflict = $this->params->get('jquery-noconflict') !== 'no';
$jqueryDetect = $this->params->get('jquery-detect') === 'yes';

//$deferTemplateJavascript = $this->params->get('defer-template-javascript') === 'yes';
//$deferCDNjavascript = $this->params->get('defer-cdn-javascript') === 'yes';

// Determine column visibility.
$hasLeftColumn = (bool)$this->countModules('content-left');
$hasRightColumn = (bool)$this->countModules('content-right');
$hasTwoColumns = $hasLeftColumn || $hasRightColumn;
$hasThreeColumns = $hasLeftColumn && $hasRightColumn;

// Switch between DIVs or a table depending on whether we're in responsive mode.
if ($responsive) {
	$contentTag1 = 'div';
	$contentTag2 = 'div';
	$contentTag3 = 'div';
} else {
	$contentTag1 = 'table';
	$contentTag2 = 'tr';
	$contentTag3 = 'td';
}

// Cache version has to be URL safe
$cacheVersion = preg_replace('/[^a-zA-Z0-9]/', '', $cacheVersion);

$classes = KarlFrameBootstrap::getBootstrapClasses($this->params);



/**
 * Determines the canonical URL for the current page.
 *
 * This will add .html to the current URL if it appears to need it.
 * It does not force HTTPS or www. because it assumes that the web server is forcing it.
 * 
 * @return  string  The URL
 */
function getCanonicalUrl()
{

	$currentUrl = JURI::current();
	$currentPath = str_replace(JURI::base(), '', JURI::current());
	$canonicalUrl  = $currentUrl;

	// Clean up slashes in URLs.
	$canonicalUrl = preg_replace('~(?<!:)//+~', '/', $canonicalUrl); // Combine multiple sequential slashes.
	$canonicalUrl = preg_replace('~/*(\.html)/*~', '$1', $canonicalUrl); // Remove any slashes in front of .html

	// Add .html when necessary.
	if ($currentPath !== '') {
		if (preg_match('/\.html$/', $canonicalUrl)) {
			$canonicalUrl = preg_replace('/\.html$/', '.html', $canonicalUrl);;
		}
		if (preg_match('(\w*\.php\/*$)', $canonicalUrl)) {
			$canonicalUrl = preg_replace('(\w*\.php\/*$)', '', $canonicalUrl);;
		} else {
			if (preg_match('~/\/*$~', $canonicalUrl)) {
				$canonicalUrl = preg_replace('~/\/*$~', '.html', $canonicalUrl);
			} else if (!preg_match('~\.html$~', $canonicalUrl)) {
				$canonicalUrl .= '.html';
			}
		}
	}

	return $canonicalUrl;
}

/**
 * Adds a script file to the HEAD. Unlike the Joomla version the script can be added first OR last.
 *
 * @param	$thisRef	A reference to the "$this" variable from the template.
 * @param	$url		The URL of the script (string).
 * @param	$first		If true or not specified the script will be first, otherwise it will be last (boolean).
 * @param	$mime		The mime type of the script (string). Default is javascript.
 */
function karlframe_addScript(&$thisRef, $library, $first = true, $mime = 'text/javascript')
{
	$script = array(
		'mime'		=> $mime,
		'defer'		=> $library->defer,
		'async'		=> false,
		'source'	=> 'karlframe'
	);
	if ($first) {
		$url = $library->url;
		$thisRef->_scripts = array($url => $script) + $thisRef->_scripts;
	} else {
		$thisRef->_scripts[$library->url] = $script;
	}
}

// IP Geo-Location.
$locationClass = '';
$js['countryCode'] = null;
$js['countryName'] = null;
if ($geolocation) {

	$geocontentHelper = JPATH_ROOT . '/modules/mod_geocontent/helper.php';
	if (file_exists($geocontentHelper)) {

		require_once $geocontentHelper;
		if (method_exists('GeoContent', 'getVersion') && version_compare(GeoContent::getVersion(), '2.2.0', 'ge')) {

			// Version 2.2.0 of GeoContent added:
			// 1. GeoContent::getVersion() which KarlFrame uses to make sure GeoContent is at a supported version.
			// 2. IpLocation::revertIsoCode() which KarlFrame uses to determine the 3-character country code (legacy KarlFrame used 3-character codes).

			$location = IpLocation::lookup();
			if ($location !== false) {
				$js['countryCode'] = $location;
				$js['countryName'] = Countries::$countries[$location];
			}

			$legacyLocation = IpLocation::revertIsoCode($location);
			if ($legacyLocation !== false) {
				$locationClass .= ' karlframe-country-' . $legacyLocation;
			}
		} else {
			throw new Exception('KarlFrame Geo-Location Error: GeoContent module is too old.');
		}
	} else {
		throw new Exception('KarlFrame Geo-Location Error: GeoContent module is missing.');
	}
}

// Responsive class.
$responsiveClass = 'non-responsive';
if ($responsive) {
	$responsiveClass = 'responsive';
}

// Check visit cookie.
$firstVisitClass = 'first-visit';
$js['firstVisit'] = true;
$visitCookieName = 'karlframe-visit' . $visitCookieSuffix;
if (isset($_COOKIE[$visitCookieName])) {
	$firstVisitClass = 'return-visit';
	$js['firstVisit'] = false;
}

// Create/update visit cookie.
$visitCookieExpirySeconds = 0;
if ($visitCookieExpiry !== '') {
	$visitCookieExpirySeconds = date('U', strtotime($visitCookieExpiry));
}
setcookie($visitCookieName, '1', $visitCookieExpirySeconds, '/');

// Modify the page title
$thisPageTitle = $frontPage ? $homePageTitle : $pageTitle;
if ($thisPageTitle != 4) {

	$title = $document->getTitle();

	$config = JFactory::getConfig();
	$siteName = method_exists($config, 'get') ? $config->get('sitename') : $config->getValue('config.sitename'); // get() exists since Joomla Platform 11.1, prior to that getValue() was used.

	switch ($thisPageTitle) {
		case 1:
			$document->setTitle($title . ' - ' . $siteName);
			break;
		case 2:
			$document->setTitle($siteName . ' - ' . $title);
			break;
		case 3:
			$document->setTitle($siteName);
			break;
	}
}

// Determine the classname(s) to apply to the BODY tag based on the URL
$urlClass = '';
$urlStrings = explode('/', preg_replace('/^\/|\.html$|\/$/', '', str_replace(JURI::base(), '', JURI::current())));
for ($i = 0; $i < count($urlStrings); $i++) {
	$buffer = $urlStrings;
	$urlClass .= ' url' . ($i == count($urlStrings) - 1 ? '' : '-part') . '_' . join('_', array_splice($buffer, 0, $i + 1));
}

// Add URL details to KarlFrame's javascript object.
$js['rootUrl'] = JURI::base();
$js['currentPath'] = str_replace(JURI::base(), '', JURI::current());

// DOCTYPE
$doctypeOption = $this->params->get('doctype');
switch ($doctypeOption) {
	case 'HTML 5':
		$doctype = '<!DOCTYPE html>';
		break;
	case 'XHTML 1.0 Strict':
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		break;
	case 'XHTML 1.1':
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
		break;
	default:
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
}
echo $doctype . "\n";

// HTML tag
switch ($doctypeOption) {
	case 'HTML 5':
		$htmlTag = '<html lang="' . $this->language . '" dir="' . $this->direction . '" >';
		break;
	default:
		$htmlTag = '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $this->language . '" lang="' . $this->language . '" dir="' . $this->direction . '" >';
}
echo $htmlTag;

?>

<head>

	<?php
	if ($gtmOpt && $gtmId) {
		//Google Optimize Mode is set to On and there is a GTM Code. Google Optimize needs a GTM Code in order to work.

		/*
		* Google Optimize.
		*/

		echo <<< EOF
	<!-- Anti-flicker snippet (recommended)  -->
	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'$gtmId':true});</script>

EOF;

		/*
		* Google Tag Manager.
		*/

		$gtmFrame = '';

		echo <<< EOF
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','{$gtmId}');</script>
	<!-- End Google Tag Manager -->

EOF;

		$gtmFrame = <<< EOF
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={$gtmId}"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

EOF;
	}

	if (strlen(trim($metaViewport)) > 0) {
		echo '  <meta name="viewport" content="' . $metaViewport . '" />' . "\n";
	}

	echo '<link rel="canonical" href="' . getCanonicalUrl() . '"/>' . "\n";

	if (file_exists(getcwd() . '/images/icon.png')) {
		echo	'  <link rel="' . $appleIconType . '" href="' . $this->baseurl . '/images/icon.png" />' . "\n" .
			'  <meta name="msapplication-TileImage" content="' . $this->baseurl . '/images/icon.png"/>' . "\n" .
			'  <meta name="msapplication-TileColor" content="' . $windowsTileColour . '"/>' . "\n";
	}

	/*
	 * Determine API base URLs and handle HTTPS
	 */

	$protocol = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '');

	$cloudflareCDN	= '//cdnjs.cloudflare.com/ajax/libs';
	$googleCDN		= '//ajax.googleapis.com/ajax/libs';


	/*
	 * Add CSS
	 */

	if ($googleFonts) {
		// @import rules only work when they're the first entry in the style. This ridiculous approach to adding the CSS ensures that no CSS gets added before the @import (ie get all the headings, modify them, set them all again).
		// It's better to add the @import here than a regular CSS file because this way we can handle HTTP vs HTTPS without needing Javascript. A protocol relative URL would work just as well, but these cause IE7 and IE8 to double-download files when used inside an @import.
		$headers = $this->getHeadData();
		$headers['style']['text/css'] = '@import url(' . $protocol . '://fonts.googleapis.com/css?family=' . $googleFonts . '); ' . (array_key_exists('text/css', $headers['style']) ? $headers['style']['text/css'] : '');
		$headers = $this->setHeadData($headers);
	}

	$document->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/styles/style.min.css?' . $cacheVersion);

	if ($menuCSS) {
		$document->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/styles/menu.min.css?' . $cacheVersion);
	}


	KarlFrameLibraries::$cacheVersion = $cacheVersion;

	$jquery = '';
	//$jqueryUrl = '';
	$bootstrap = '';
	$libraries = new KarlFrameLibraries($scripts);


	if (!$libraries->jqueryFromJoomla) {
		unset($this->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
		unset($this->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
		unset($this->_scripts[JURI::root(true) . '/media/jui/js/jquery-migrate.min.js']);
	}
	if (!$libraries->bootstrapFromJoomla) {
		unset($this->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);
	}
	if ($coreJs != 'joomla') {
		unset($this->_scripts[JURI::root(true) . '/media/system/js/core.js']);
		unset($this->_scripts[JURI::root(true) . '/media/system/js/core-uncompressed.js']);
	}
	if ($capJs != 'joomla') {
		unset($this->_scripts[JURI::root(true) . '/media/system/js/caption.js']);
		unset($this->_scripts[JURI::root(true) . '/media/system/js/caption-uncompressed.js']);
		if (isset($this->_script['text/javascript'])) {

			// Newer versions of Joomla (definitely version 3.4).
			$this->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\([^}]+JCaption.*?}\);\s*%s', '', $this->_script['text/javascript']);

			// Older versions of Joomla.
			$this->_script['text/javascript'] = preg_replace('%window\.addEvent\(\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%', '', $this->_script['text/javascript']);

			if (empty($this->_script['text/javascript'])) {
				unset($this->_script['text/javascript']);
			}
		}
	}

	if ($respondjs != '' && $respondjs != 'no') {
		list($source, $version) = explode('-', $respondjs);
		switch ($source) {
			case 'cloudflare':
				$url = "$cloudflareCDN/respond.js/$version/respond.min.js";
				$url2 = "$cloudflareCDN/respond.js/$version/respond-proxy.html";
				break;
		}
		karlframe_addScript($this, new KarlFrameResource(KarlFrameResource::SCRIPT, $this->baseurl . '/templates/karlframe/scripts/respond.proxy.js'));
		$document->addHeadLink($this->baseurl . '/templates//karlframe/scripts/respond.proxy.gif', 'respond-direct', 'rel', array('id' => 'respond-redirect'));
		if ($url) {
			karlframe_addScript($this, new KarlFrameResource(KarlFrameResource::SCRIPT, $url));
		}
		if ($url2) {
			$document->addHeadLink($url2, 'respond-proxy', 'rel', array('id' => 'respond-proxy'));
		}
	}

	if ($jqueryUI != '' && $jqueryUI != '0') {
		list($source, $version) = explode('-', $jqueryUI);
		switch ($source) {
			case 'cloudflare':
				$url = "$cloudflareCDN/jqueryui/$version/jquery-ui.min.js";
				break;
			case 'google':
				$url = "$googleCDN/jqueryui/$version/jquery-ui.min.js";
				break;
		}
		if ($url) {
			karlframe_addScript($this, new KarlFrameResource(KarlFrameResource::SCRIPT, $url));
		}

		if ($jqueryUItheme != '' && $jqueryUItheme != '0') {
			list($source, $theme) = explode('-', $jqueryUItheme, 2);
			switch ($source) {
				case 'google':
					$url = "$googleCDN/jqueryui/$version/themes/$theme/jquery-ui.css";
					break;
			}
			if ($url) {
				$document->addStyleSheet($url);
			}
		}
	}



	// The styles are added first in the order they were defined in the template scripts field
	foreach ($libraries->styles as $library) {
		$document->addStyleSheet($library->url);
	}

	// Since each script gets added to the top (stack vs queue) they must be reversered and then added in order to stay above any scripts added by other extensions
	foreach (array_reverse($libraries->scripts) as $library) {
		karlframe_addScript($this, $library);
	}

	if ($karlframeJS != '' && $karlframeJS != '0') {
		$karlframeJSpath = '/templates/' . $this->template . '/scripts/karlframe.min.js';
		karlframe_addScript($this, new KarlFrameResource(KarlFrameResource::SCRIPT, $this->baseurl . $karlframeJSpath . '?' . $cacheVersion), false);
	}


	if ($jqueryNoConflict) {
		$document->addScriptDeclaration('if ($ && $.noConflict) { $.noConflict(); }', 'text/javascript');
	}

	// KarlFrame javascript object.
	printf('<script type="text/javascript">var KarlFrame=(function(){return%s}())</script>', json_encode($js));
	?>

	<jdoc:include type="head" />

	<?php

	/**
	 *Add Social Media Links
	 */
	if ($socialProfiles && $frontPage) {
		$socials = [];
		foreach (explode("\n", $socialProfiles) as $url) {
			if (trim($url)) {
				$socials[] = trim($url);
			}
		}
		echo '<script type="application/ld+json">
	{	"@context" : "http://schema.org",
		"@type": "Organization",
		"name" : "' . $siteName . '",
		"url" : "' . JURI::base() . '",
		"logo" : "' . $logo . '",
		"sameAs" : ["' . join('","', $socials) . '"]
	}
</script>' . "\n";
	}

	/**
	 * Favicon Code
	 */
	if ($faviconCode) {
		echo <<< EOF
<!-- Favicons -->

EOF;
		echo $faviconCode;
		echo <<<EOF
	
<!-- End Favicons -->

EOF;
	}

	if (!$gtmOpt) {
		/*
     * Google Tag Manager.
     */
		$gtmFrame = '';
		if ($gtmId) {
			echo <<< EOF
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{$gtmId}');</script>
    <!-- End Google Tag Manager -->
    
EOF;
			$gtmFrame = <<< EOF
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={$gtmId}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
EOF;
		}
	}

	/*
 * IE6 fixes for popup menu's and PNG transparency
 */
	if ($ie6Fixes) {
		echo <<<EOF
		<!--[if lte IE 6]>
			<style type="text/css">
				/* Alpha PNG Fixes - based on http://www.twinhelix.com/css/iepngfix/ */
				$ie6AlphaSelectors { behavior: url({$this->baseurl}/templates/{$this->template}/styles/iepngfix.htc); }
				$ie6StaticAlphaSelectors { position:static; behavior: url({$this->baseurl}/templates/{$this->template}/styles/iepngfix.htc); }
				ul.popup-menu ul { margin-left:auto; margin-top:0; } /*Added as temp fix for pop-up menus*/
			</style>
			<script type="text/javascript" src="{$this->baseurl}/templates/{$this->template}/scripts/ie6.js"></script>
			<script type="text/javascript">window.attachEvent("onload", sfHover);</script>
		<![endif]-->
EOF;
	}

	/*
 * CSS3 PIE
 */
	if ($css3pieSelectors) {
		echo '<!--[if IE]><style type="text/css">' . $css3pieSelectors . ' { behavior: url(' . $this->baseurl . '/templates/' . $this->template . '/scripts/PIE.htc); }</style><![endif]-->';
	}


	/*
 * IE CSS (conditional comments)
 */
	if ($ieCSS) {
		$ieFiles = glob(getcwd() . '/templates/' . $this->template . '/styles/ie*.css');
		foreach ($ieFiles as $ieFile) {
			$condition = '';
			$ieFile = basename($ieFile);
			if ($ieFile == 'ie.css') {
				$condition = 'IE';
			} else if (preg_match('/ie(\d+)([gl]te?)?\.css/', $ieFile, $m)) {
				$condition = (count($m) > 2 ? $m[2] . ' ' : '') . 'IE ' . $m[1];
			}
			if ($condition) {
				echo '<!--[if ' . $condition . ']><link type="text/css" rel="stylesheet" href="' . $this->baseurl . '/templates/' . $this->template . '/styles/' . $ieFile . '?' . $cacheVersion . '" /><![endif]-->' . "\n";
			}
		}
	}

	if ($analyticsID) {
		switch ($analyticsMode) {

			case 'classic':

				/*
			 * Google Analytics Tracker.
			 */

				if ($analyticsID != '') {
					$push = array();
					$IDs = preg_split('/\s+/', $analyticsID);
					for ($i = 0; $i < count($IDs); $i++) {
						$prefix = $i > 0 ? 't' . ($i + 1) . '.' : '';
						$push[] = "['{$prefix}_setAccount', '" . $IDs[$i] . "']," . (count($IDs) == 1 ? "\n\t\t\t\t['_setAllowLinker', true],\n\t\t\t\t['_setDomainName', '" . $domain . "']," : "\n\t\t\t\t['{$prefix}_setAllowLinker', true],\n\t\t\t\t['_setDomainName', '" . $domain . "'],") . "\n\t\t\t\t['{$prefix}_trackPageview']";
					}
					$push = join(",\n\t\t\t\t", $push);

					echo <<<EOF
					<script type="text/javascript">
						try {
							var _gaq = _gaq || [];
							_gaq.push(
								$push
							);
							(function() {
								var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
								ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
								var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
							})();
						} catch(e) {}
					</script>
EOF;
				}

				/*
			 * Google Analytics Cross Domain.
			 */
				if ($analyticsCrossDomain != '') {
					$formSelector = 'form[method="post"][action*="' . join('"], form[method="post"][action*="', preg_split('/\s+/', $analyticsCrossDomain)) . '"]';
					$linkSelector = 'a[href*="' . join('"], a[href*="', preg_split('/\s+/', $analyticsCrossDomain)) . '"]';
					echo <<<EOF
					<script type="text/javascript">
						jQuery(function() {
							jQuery('$linkSelector').click(function() { _gaq.push(['_link', this.href]); return false; });
							jQuery('$formSelector').submit(function() { _gaq.push(['_linkByPost', this]); });
						});
					</script>
EOF;
				}

				break; // End of Classic Mode.

			case 'universal':

				/*
			* Google Universal Analytics.
			*/

				$allowGALinker = $this->params->get("ga-linker", "0");
				$enableDispFeat = $this->params->get("ga-disp-feat", "0");
				$enableLinkID = $this->params->get("ga-linkid", "0");
				$gaDoms = '';
				if ($analyticsCrossDomain != '') {
					$gaDoms = preg_replace("/\s+/", "','", $analyticsCrossDomain);
				}
				$allowLinker = ($allowGALinker == '1') ? ", {'allowLinker': true}" : "";
				$linker = ($allowGALinker == '1') ? "\n ga('require', 'linker');" : "";
				$autoLink = ($allowGALinker == '1') ? "\n ga('linker:autoLink', ['$gaDoms'], false, true);" : "";
				$linkid = ($enableLinkID == '1') ? "\n ga('require', 'linkid', 'linkid.js');" : "";
				$dispfeat = ($enableDispFeat == '1') ? "\n ga('require', 'displayfeatures');" : "";

				echo <<<EOF
		<script type="text/javascript">
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', '$analyticsID', 'auto' $allowLinker); {$dispfeat}{$linkid}{$linker}{$autoLink}
			ga('send', 'pageview');
		</script>
EOF;

				break; // End of Universal Mode.

		} // End of switch statement for Analytics.
	}

	if ($carousel) {
		echo '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/' . $slickCarouselVersion . '/slick.min.js"></script>';
		echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/' . $slickCarouselVersion . '/slick-theme.min.css">';
		echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/' . $slickCarouselVersion . '/slick.min.css">';
	}

	?>

</head>
<?php
$currentPageUrl = JUri::getInstance();
$cookieBodyClass = "";
foreach (preg_split('/$/m', $cookieData) as $customCookie) {
	if ($customCookie) {
		$cookieDetails = preg_split('/\s\|\s/', $customCookie);
		if (preg_match('~' . $cookieDetails[0] . '~', $currentPageUrl)) {
			setcookie("$cookieDetails[1]", "$cookieDetails[2]");
			$cookieBodyClass .= " " . $cookieDetails[3];
		} else if ($_COOKIE[$cookieDetails[1]] == $cookieDetails[2]) {
			$cookieBodyClass .= " " . $cookieDetails[3];
		}
	}
}
?>

<body <?php echo 'class="' . ($frontPage ? 'front-page' : "other-page $urlClass") . ' ' . $responsiveClass . ' ' . $firstVisitClass . ' ' . $locationClass . ' ' . $cookieBodyClass . '"'; /* If the page is the front page it will have a class of "front-page", otherwise it will have a class of based on the address of the page and a class of "other-page". */ ?>>

	<?= $gtmFrame; ?>

	<div id="body">

		<div class="body-section <?php echo $classes['body-header']; ?>" id="body-header">
			<div class="header-section <?php echo $classes['body-header-1']; ?>" id="body-header-1">
				<div class="header-box">
					<div class="header-sub">
						<jdoc:include type="modules" name="body-header-1" />
					</div>
				</div>
			</div>
			<div class="header-section <?php echo $classes['body-header-2']; ?>" id="body-header-2">
				<div class="header-box">
					<div class="header-sub">
						<jdoc:include type="modules" name="body-header-2" />
					</div>
				</div>
			</div>
			<div class="header-section <?php echo $classes['body-header-3']; ?>" id="body-header-3">
				<div class="header-box">
					<div class="header-sub">
						<jdoc:include type="modules" name="body-header-3" />
					</div>
				</div>
			</div>
		</div>

		<div class="body-section" id="page">

			<div class="page-section" id="page-header">
				<div class="header-section <?php echo $classes['page-header-3']; ?>" id="page-header-1">
					<div class="header-box">
						<div class="header-sub">
							<jdoc:include type="modules" name="page-header-1" />
						</div>
					</div>
				</div>
				<div class="header-section <?php echo $classes['page-header-3']; ?>" id="page-header-2">
					<div class="header-box">
						<div class="header-sub">
							<jdoc:include type="modules" name="page-header-2" />
						</div>
					</div>
				</div>
				<div class="header-section <?php echo $classes['page-header-3']; ?>" id="page-header-3">
					<div class="header-box">
						<div class="header-sub">
							<jdoc:include type="modules" name="page-header-3" />
						</div>
					</div>
				</div>
			</div>

			<div class="page-section <?php echo $classes['page']; ?>" id="content">
				<div id="content-box">
					<div id="content-sub">
						<<?= $contentTag1; ?> id="content-columns" class="<?php echo $hasThreeColumns ? 'three-column' : ($hasTwoColumns ? 'two-column' : 'one-column'); ?>">
							<<?= $contentTag2; ?> id="content-columns-sub" class=" <?= $classes['content-columns-sub']; ?>">

								<?php if ($hasLeftColumn) : ?>
									<<?= $contentTag3; ?> id="content-left" class="content-cell <?= $classes['content-left']; ?>">
										<div id="content-left-sub" class="content-div">
											<jdoc:include type="modules" name="content-left" />
										</div>
									</<?= $contentTag3; ?>>

								<?php endif; ?>
								<<?= $contentTag3; ?> id="content-center" class="content-cell <?= $classes['content-center']; ?>">
									<div id="content-center-sub" class="content-div">
										<div id="content-top" class=" <?= $classes['content-top']; ?>">
											<jdoc:include type="modules" name="content-top" />
										</div>
										<div id="content-component" class=" <?= $classes['content-component']; ?>">
											<jdoc:include type="component" />
										</div>
										<div id="content-bottom" class=" <?= $classes['content-bottom']; ?>">
											<jdoc:include type="modules" name="content-bottom" />
										</div>
									</div>
								</<?= $contentTag3; ?>>

								<?php if ($hasRightColumn) : ?>
									<<?= $contentTag3; ?> id="content-right" class="content-cell <?= $classes['content-right']; ?>">
										<div id="content-right-sub" class="content-div">
											<jdoc:include type="modules" name="content-right" />
										</div>
									</<?= $contentTag3; ?>>
								<?php endif; ?>

							</<?= $contentTag2; ?>>
						</<?= $contentTag1; ?>>
					</div>
				</div>
			</div>

			<div class="page-section" id="page-footer">
				<div class="footer-section <?php echo $classes['page-footer-1']; ?>" id="page-footer-1">
					<div class="footer-box">
						<div class="footer-sub">
							<jdoc:include type="modules" name="page-footer-1" />
						</div>
					</div>
				</div>
				<div class="footer-section <?php echo $classes['page-footer-2']; ?>" id="page-footer-2">
					<div class="footer-box">
						<div class="footer-sub">
							<jdoc:include type="modules" name="page-footer-2" />
						</div>
					</div>
				</div>
				<div class="footer-section <?php echo $classes['page-footer-3']; ?>" id="page-footer-3">
					<div class="footer-box">
						<div class="footer-sub">
							<jdoc:include type="modules" name="page-footer-3" />
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="body-section  <?php echo $classes['body-footer']; ?>" id="body-footer">
			<div class="footer-section <?php echo $classes['body-footer-1']; ?>" id="body-footer-1">
				<div class="footer-box">
					<div class="footer-sub">
						<jdoc:include type="modules" name="body-footer-1" />
					</div>
				</div>
			</div>
			<div class="footer-section <?php echo $classes['body-footer-2']; ?>" id="body-footer-2">
				<div class="footer-box">
					<div class="footer-sub">
						<jdoc:include type="modules" name="body-footer-2" />
					</div>
				</div>
			</div>
			<div class="footer-section <?php echo $classes['body-footer-3']; ?>" id="body-footer-3">
				<div class="footer-box">
					<div class="footer-sub">
						<jdoc:include type="modules" name="body-footer-3" />
					</div>
				</div>
			</div>
		</div>

	</div>

</body>

</html>