<?php

jimport('joomla.application.module.helper');

/**
 * Assists with GeoContent implementations.
 */
class KarlFrameGeoContent
{

    static $country;
    static $region;
    static $items;

    /**
     * Constructor.
     * @throws UnexpectedValueException If it cannot be determined whether a library is a script or a style.
     */
    public static function connectGeo()
    {
        $filname = JPATH_ROOT . '/modules/mod_geocontent';

        if (file_exists($filname)) {
            include_once JPATH_ROOT . '/modules/mod_geocontent/helper.php';
        } else {
            //Throw exception
            throw new Exception('KarlFrameGeoContent failed to include ' . JPATH_ROOT . '/modules/mod_geocontent/helper.php');
        }
    }

    /**
     * Calls GeoContent IpLocation function and 
     * @param   integer  $index  The new value.
     */
    public static function getCurrentCountryCode()
    {
        self::$country = IpLocation::lookup();

        if (!self::$country) {
            self::$country = "all";
            self::$region = "all";
        } else {
            // Country is set
            $regionsLists = Countries::$regions; // Multidimensional array of regions from GeoContent;
            $regionNamesList = array_keys(Countries::$regionNames);

            for ($i = 0; $i < count($regionsLists); $i++) {
                if (in_array(self::$country, $regionsLists[$regionNamesList[$i]])) {
                    // I don't know why '$regionsLists[$regionNamesList[$i]]' is appearing as a syntax error.
                    self::$region = $regionNamesList[$i];
                }
            }

            if (empty(self::$region)) {
                throw new Exception('KarlFrameGeoContent failed to find the current region.');
            }
        }
    }

    /**
     * Determines whether the current country matches any of the codes passed
     * 
     * @param   string|string[]  $codes A single country code or an array of country codes 
     * @return  boolean          Whether the current country matches any of the specified codes
     */
    public static function matchCountryCode($codes)
    {

        $match = false;

        if (empty($codes) || ($codes == "all" || in_array("all", $codes))) {
            $match = true; // No codes are set so all applies here
        } else if (is_array($codes)) {
            if (in_array(static::$country, $codes) || in_array(static::$region, $codes)) {
                $match = true;
            }
        } else if (is_string($codes)) {
            if (static::$country == $codes || static::$region == $codes) {
                $match = true;
            }
        } else {
            $match = false;
        }

        return $match;
    }
}
