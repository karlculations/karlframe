<?php
// No direct access.
defined('_JEXEC') or die;
define('DS', DIRECTORY_SEPARATOR);

// Hide the generator so hackers can't easily see our Joomla version.
$this->setGenerator('');

// Get a reference to the current document. This will be useful later.
$document = JFactory::getDocument();

// Determine if we're on the home page (the active menu item will be the default menu item)
$menu = JFactory::getApplication()->getMenu();
$frontPage = ($menu->getActive() == $menu->getDefault());

// Determine domain (currently only used for Analytics)
$domain = preg_replace('/^www\.|[^a-zA-Z0-9._-]/', '', $_SERVER['SERVER_NAME'] ?: $_SERVER['HTTP_HOST']);

// Detmine values for text variables
$pageTitle = $this->params->get('page-title');
$homePageTitle = $this->params->get('home-page-title');
$leftColumn = $this->params->get('left-column');
$rightColumn = $this->params->get('right-column');
$emptyColumns = $this->params->get('empty-columns');
$analyticsID = $this->params->get('analytics-id');
$analyticsCrossDomain = $this->params->get('analytics-cross-domain');
$analyticsID2 = $this->params->get('analytics-id-2');

$otherCSS = $this->params->get('other-css');
$otherLESS = $this->params->get('other-less');
$otherJS = $this->params->get('other-javascript');
$ie6AlphaSelectors = $this->params->get('ie6-alpha-selectors');
$ie6StaticAlphaSelectors = $this->params->get('ie6-static-alpha-selectors');
$css3pieSelectors = $this->params->get('css3-pie-selectors');
$googleFonts = $this->params->get('google-fonts');
//$jquery = $this->params->get('jquery');
$jquery = '';
$jqueryColor = $this->params->get('jquery-color');
$jqueryUI = $this->params->get('jquery-ui');
$jqueryUItheme = $this->params->get('jquery-ui-theme');
$capJs = $this->params->get('captionjs');
$coreJs = $this->params->get('corejs');
$prototype = $this->params->get('prototype');
$ext = $this->params->get('ext');
$firebugLite = $this->params->get('firebug-lite');
$html5shiv = $this->params->get('html5shiv');
$dojo = $this->params->get('dojo');
$chromeFrame = $this->params->get('chrome-frame');
$modernizr = $this->params->get('modernizr');
$yui = $this->params->get('yui');
$cacheVersion = $this->params->get('cache-version');
$metaViewport = $this->params->get('meta-viewport');
$appleIconType = $this->params->get('apple-icon-type');
$windowsTileColour = $this->params->get('windows-tile-colour');
$countryWhitelist = $this->params->get('country-whitelist');

$scriptType = 'text/javascript';

// Detmine boolean values for checkbox variables
// The variation between 'no' and 'yes' is to better support the change from checkboxes to radio buttons.
$menuCSS = $this->params->get('menu-css') !== 'no';
$customCSS = $this->params->get('custom-css') !== 'no';
$geolocation = $this->params->get('geolocation') === 'yes';
$ie6Fixes = $this->params->get('ie6-fixes') !== 'no';
$ieCSS = $this->params->get('ie-css') !== 'no';
$css3pie = $this->params->get('css3-pie') !== 'no';
$jqueryNoConflict = $this->params->get('jquery-noconflict') !== 'no';
$jqueryDetect = $this->params->get('jquery-detect') === 'yes';
//$deferTemplateJavascript = $this->params->get('defer-template-javascript') === 'yes';
//$deferCDNjavascript = $this->params->get('defer-cdn-javascript') === 'yes';

// The "enable" variables are based purely on the template parameters and whether we're on the front page or not.
$enableLeftColumn = $leftColumn == 1 || ($leftColumn == 2 && !$frontPage) || ($leftColumn == 3 && $frontPage);
$enableRightColumn = $rightColumn == 1 || ($rightColumn == 2 && !$frontPage) || ($rightColumn == 3 && $frontPage);

// The "has" variables take into account where there's actually anything to show. Empty columns may be hidden.
$hasLeftColumn = $enableLeftColumn && ($this->countModules('content-left') || $emptyColumns == 3 || $emptyColumns == 4);
$hasRightColumn = $enableRightColumn && ($this->countModules('content-right') || $emptyColumns == 2 || $emptyColumns == 4);
$hasTwoColumns = $hasLeftColumn || $hasRightColumn;
$hasThreeColumns = $hasLeftColumn && $hasRightColumn;

// Cache version has to be URL safe
$cacheVersion = preg_replace('/[^a-zA-Z0-9]/', '', $cacheVersion);

// IP Geo-Location
$locationClass = '';
if ($geolocation) {
	@require_once 'class.ip2location.php';
	$ip2l = new KarlFrame_IP2Location(KARLFRAME_IP2L_3CHAR);
	$location = $ip2l->getLocation();

	$locationClass = 'karlframe-country';
	if ($location && (strlen(trim($countryWhitelist)) == 0 || strstr($countryWhitelist, $location))) {
		$locationClass .= ' karlframe-country-' . $location;
	} else {
		$locationClass .= ' karlframe-country-unknown';
	}
}

// Modify the page title
$thisPageTitle = $frontPage ? $homePageTitle : $pageTitle;
if ($thisPageTitle != 4) {

	$title = $document->getTitle();

	$config = &JFactory::getConfig();
	$siteName = method_exists($config, 'get') ? $config->get('sitename') : $config->getValue('config.sitename'); // get() exists since Joomla Platform 11.1, prior to that getValue() was used.

	switch ($thisPageTitle) {
		case 1:
			$document->setTitle($title . ' - ' . $siteName);
			break;
		case 2:
			$document->setTitle($siteName . ' - ' . $title);
			break;
		case 3:
			$document->setTitle($siteName);
			break;
	}
}

// Determine the classname to apply to the BODY tag based on the URL
$urlClass = 'url_' . str_replace('/', '-', preg_replace('/^\/|\.html$|\/$/', '', str_replace(JURI::base(), '', JURI::current())));
$urlStrings = explode('/', preg_replace('/^\/|\.html$|\/$/', '', str_replace(JURI::base(), '', JURI::current())));
for ($i = 0; $i < count($urlStrings); $i++) {
	$buffer = $urlStrings;
	$urlClass .= ' url' . ($i == count($urlStrings) - 1 ? '' : '-part') . '_' . join('_', array_splice($buffer, 0, $i + 1));
}

//Url Replace Snippet
$urlReplace = <<<EOF
	jQuery(document).ready(function() {
		var url = jQuery(location).attr('href');
		if(!(url.search('/?tmpl=component') == -1)){
			jQuery('a').each(function()
			{
				/*var link = jQuery(this).attr('href');
				link = link.replace('.html','.html?tmpl=component');
				jQuery(this).attr('href',link);
				jQuery(this).attr('target','_parent');*/
			});
		}
	});
EOF;

// DOCTYPE
$doctype = $this->params->get('doctype');
switch ($doctype) {
	case 'HTML 5':
		$doctype = '<!DOCTYPE html>';
		break;
	case 'XHTML 1.0 Strict':
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		break;
	case 'XHTML 1.1':
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
		break;
	default:
		$doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
}
echo $doctype;
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
	<?php
	if (strlen(trim($metaViewport)) > 0) {
		echo '  <meta name="viewport" content="' . $metaViewport . '" />' . "\n";
	}

	if (file_exists(getcwd() . '/images/icon.png')) {
		echo	'  <link rel="' . $appleIconType . '" href="' . $this->baseurl . '/images/icon.png" />' . "\n" .
			'  <meta name="msapplication-TileImage" content="' . $this->baseurl . '/images/icon.png"/>' . "\n" .
			'  <meta name="msapplication-TileColor" content="' . $windowsTileColour . '"/>' . "\n";
	}

	/*
			 * Determine API base URLs and handle HTTPS
			 */

	$protocol = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '');

	$cloudflareCDN	= '//cdnjs.cloudflare.com/ajax/libs';
	$googleCDN		= '//ajax.googleapis.com/ajax/libs';
	$microsoftCDN	= '//ajax.aspnetcdn.com/ajax';
	$karlframeCDN	= $this->baseurl . '/templates/' . $this->template . '/scripts';

	/*
			 * Add CSS
			 */

	if ($googleFonts) {
		// @import rules only work when they're the first entry in the style. This ridiculous approach to adding the CSS ensures that no CSS gets added before the @import (ie get all the headings, modify them, set them all again).
		// It's better to add the @import here than a regular CSS file because this way we can handle HTTP vs HTTPS without needing Javascript. A protocol relative URL would work just as well, but these cause IE7 and IE8 to double-download files when used inside an @import.
		$headers = $this->getHeadData();
		$headers['style']['text/css'] = '@import url(' . $protocol . '://fonts.googleapis.com/css?family=' . $googleFonts . '); ' . (array_key_exists('text/css', $headers['style']) ? $headers['style']['text/css'] : '');
		$headers = $this->setHeadData($headers);
	}

	$document->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/styles/style.min.css?' . $cacheVersion);

	if ($menuCSS) {
		$document->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/styles/menu.min.css?' . $cacheVersion);
	}

	if ($customCSS) {
		$customCSSfile = '/templates/' . $this->template . '/styles/custom.css';
		if (file_exists(getcwd() . $customCSSfile)) {
			$document->addStyleSheet($this->baseurl . $customCSSfile . '?' . $cacheVersion);
		}
	}

	if ($otherCSS != '') {
		$otherCSSfiles = preg_split('/ *, */', $otherCSS);
		foreach ($otherCSSfiles as $otherCSSfile) {
			$otherCSSpath = '/templates/' . $this->template . '/styles/' . $otherCSSfile . '.css';
			if (file_exists(getcwd() . $otherCSSpath)) {
				$document->addStyleSheet($this->baseurl . $otherCSSpath . '?' . $cacheVersion);
			}
		}
	}

	if ($otherLESS != '') {
		$otherLESSfiles = preg_split('/ *, */', $otherLESS);
		foreach ($otherLESSfiles as $otherLESSfile) {
			$otherLESSpath = '/templates/' . $this->template . '/styles/' . $otherLESSfile . '.less';
			if (file_exists(getcwd() . $otherLESSpath)) {
				$document->addStyleSheet($this->baseurl . $otherLESSpath . '?' . $cacheVersion, 'text/less');
			}
		}
	}

	$removeScripts = array();
	if ($coreJs != 'joomla') {
		unset($this->_scripts[JURI::root(true) . '/media/system/js/core.js']);
		unset($this->_scripts[JURI::root(true) . '/media/system/js/core-uncompressed.js']);
	}
	if ($capJs != 'joomla') {
		unset($this->_scripts[JURI::root(true) . '/media/system/js/caption.js']);
		unset($this->_scripts[JURI::root(true) . '/media/system/js/caption-uncompressed.js']);
		if (isset($this->_script['text/javascript'])) {
			$this->_script['text/javascript'] = preg_replace('%window\.addEvent\(\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%', '', $this->_script['text/javascript']);
			if (empty($this->_script['text/javascript'])) {
				unset($this->_script['text/javascript']);
			}
		}
	}

	if ($otherLESS != '') {
		$lessPath = '/templates/' . $this->template . '/scripts/less-1.3.3.min.js';
		if (file_exists(getcwd() . $lessPath)) {
			$document->addScript($this->baseurl . $lessPath);
		}
	}

	if ($jquery != '' && $jquery != '0') {
		$includeJquery = true;
		if ($jqueryDetect) {
			$headers = $this->getHeadData();
			/* only import jquery if something else has not already imported it */
			foreach ($headers['scripts'] as $key => $value) {
				if (preg_match('{/jquery[^/]*.js$}', $key)) {
					$includeJquery = false;
					break;
				}
			}
		}
		if ($includeJquery) {
			list($source, $version) = explode('-', $jquery);
			switch ($source) {
				case 'cloudflare':
					$url = "$cloudflareCDN/jquery/$version/jquery.min.js";
					break;
				case 'google':
					$url = "$googleCDN/jquery/$version/jquery.min.js";
					break;
				case 'microsoft':
					$url = "$microsoftCDN/jquery/jquery-$version.min.js";
					break;
			}
			if ($url) {
				$document->addScript($url);
			}
			if ($jqueryNoConflict) {
				$document->addScriptDeclaration('$.noConflict();', 'text/javascript');
			}
		}
		$document->addScriptDeclaration($urlReplace, 'text/javascript');
	}

	if ($jqueryColor != '' && $jqueryColor != '0') {
		list($source, $version) = explode('-', $jqueryColor);
		switch ($source) {
			case 'cloudflare':
				$url = "$cloudflareCDN/jquery-color/$version/jquery.color.min.js";
				break;
			case 'karlframe':
				$url = "$karlframeCDN/jquery.color-2.1.1.min.js";
				break;
		}
		if ($url) {
			$document->addScript($url);
		}
	}

	if ($jqueryUI != '' && $jqueryUI != '0') {
		list($source, $version) = explode('-', $jqueryUI);
		switch ($source) {
			case 'cloudflare':
				$url = "$cloudflareCDN/jqueryui/$version/jquery-ui.min.js";
				break;
			case 'google':
				$url = "$googleCDN/jqueryui/$version/jquery-ui.min.js";
				break;
			case 'microsoft':
				$url = "$microsoftCDN/jquery.ui/$version/jquery-ui.min.js";
				break;
		}
		if ($url) {
			$document->addScript($url);
		}

		if ($jqueryUItheme != '' && $jqueryUItheme != '0') {
			list($source, $theme) = explode('-', $jqueryUItheme, 2);
			switch ($source) {
				case 'google':
					$url = "$googleCDN/jqueryui/$version/themes/$theme/jquery-ui.css";
					break;
				case 'microsoft':
					$url = "$microsoftCDN/jquery.ui/$version/themes/$theme/jquery-ui.css";
					break;
			}
			if ($url) {
				$document->addStyleSheet($url);
			}
		}
	}

	if ($chromeFrame != '' && $chromeFrame != '0') {
		list($source, $version) = explode('-', $chromeFrame);
		$document->addScript("$googleCDN/chrome-frame/$version/CFInstall.min.js");
	}

	if ($dojo != '' && $dojo != '0') {
		list($source, $version) = explode('-', $dojo);
		$document->addScript("$googleCDN/dojo/$version/dojo/dojo.xd.js");
	}

	if ($ext != '' && $ext != '0') {
		list($source, $version) = explode('-', $ext);
		$document->addScript("$googleCDN/ext-core/$version/ext-core.js");
	}

	if ($firebugLite != '' && $firebugLite != '0') {
		list($source, $version) = explode('-', $firebugLite);
		$document->addScript("$cloudflareCDN/firebug-lite/$version/firebug-lite.js");
	}

	if ($html5shiv != '' && $html5shiv != '0') {
		list($source, $version) = explode('-', $html5shiv);
		$document->addScript("$cloudflareCDN/html5shiv/$version/html5shiv.js");
	}

	if ($modernizr != '' && $modernizr != '0') {
		list($source, $version) = explode('-', $modernizr);
		$document->addScript("$cloudflareCDN/modernizr/$version/modernizr.min.js");
	}

	if ($prototype != '' && $prototype != '0') {
		list($source, $version) = explode('-', $prototype);
		$document->addScript("$googleCDN/prototype/$version/prototype.js");
	}

	if ($yui != '' && $yui != '0') {
		list($source, $version) = explode('-', $yui);
		$document->addScript("$googleCDN/yui/$version/build/yui/yui-min.js");
	}

	if ($otherJS != '') {
		$otherJSfiles = preg_split('/ *, */', $otherJS);
		foreach ($otherJSfiles as $otherJSfile) {
			$otherJSpath = '/templates/' . $this->template . '/scripts/' . $otherJSfile . '.js';
			if (file_exists(getcwd() . $otherJSpath)) {
				$document->addScript($this->baseurl . $otherJSpath . '?' . $cacheVersion);
			}
		}
	}

	?>
	<jdoc:include type="head" />

	<?php

	/*
 * IE6 fixes for popup menu's and PNG transparency
 */
	if ($ie6Fixes) {
		echo <<<EOF
		<!--[if lte IE 6]>
			<style type="text/css">
				/* Alpha PNG Fixes - based on http://www.twinhelix.com/css/iepngfix/ */
				$ie6AlphaSelectors { behavior: url({$this->baseurl}/templates/{$this->template}/styles/iepngfix.htc); }
				$ie6StaticAlphaSelectors { position:static; behavior: url({$this->baseurl}/templates/{$this->template}/styles/iepngfix.htc); }
				ul.popup-menu ul { margin-left:auto; margin-top:0; } /*Added as temp fix for pop-up menus*/
			</style>
			<script type="text/javascript" src="{$this->baseurl}/templates/{$this->template}/scripts/ie6.js"></script>
			<script type="text/javascript">window.attachEvent("onload", sfHover);</script>
		<![endif]-->
EOF;
	}

	/*
 * CSS3 PIE
 */
	if ($css3pieSelectors) {
		echo '<!--[if IE]><style type="text/css">' . $css3pieSelectors . ' { behavior: url(' . $this->baseurl . '/templates/' . $this->template . '/scripts/PIE.htc); }</style><![endif]-->';
	}


	/*
 * IE CSS (conditional comments)
 */
	if ($ieCSS) {
		$ieFiles = glob(getcwd() . '/templates/' . $this->template . '/styles/ie*.css');
		foreach ($ieFiles as $ieFile) {
			$condition = '';
			$ieFile = basename($ieFile);
			if ($ieFile == 'ie.css') {
				$condition = 'IE';
			} else if (preg_match('/ie(\d+)([gl]te?)?\.css/', $ieFile, $m)) {
				$condition = (count($m) > 2 ? $m[2] . ' ' : '') . 'IE ' . $m[1];
			}
			if ($condition) {
				echo '<!--[if ' . $condition . ']><link type="text/css" rel="stylesheet" href="' . $this->baseurl . '/templates/' . $this->template . '/styles/' . $ieFile . '?' . $cacheVersion . '" /><![endif]-->' . "\n";
			}
		}
	}

	/*
 * Google Analytics Tracker
 */
	if ($analyticsID != '') {
		$push = array();
		$IDs = preg_split('/\s+/', $analyticsID);
		for ($i = 0; $i < count($IDs); $i++) {
			$prefix = $i > 0 ? 't' . ($i + 1) . '.' : '';
			$push[] = "['{$prefix}_setAccount', '" . $IDs[$i] . "']," . (count($IDs) == 1 ? '' : "\n\t\t\t\t['{$prefix}_setAllowLinker', true],\n\t\t\t\t['_setDomainName', '" . $domain . "'],") . "\n\t\t\t\t['{$prefix}_trackPageview']";
		}
		$push = join(",\n\t\t\t\t", $push);

		echo <<<EOF
		
		<script type="text/javascript">
			try {
				var _gaq = _gaq || [];
				_gaq.push(
					$push
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			} catch(e) {}
		</script>
EOF;
	}

	/*
 * Google Analytics Cross Domain
 */
	if ($analyticsCrossDomain != '') {
		$formSelector = 'form[method="post"][action*="' . join('"], form[method="post"][action*="', preg_split('/\s+/', $analyticsCrossDomain)) . '"]';
		$linkSelector = 'a[href*="' . join('"], a[href*="', preg_split('/\s+/', $analyticsCrossDomain)) . '"]';
		echo <<<EOF
		<script type="text/javascript">
			jQuery(function() {
				/*jQuery('$linkSelector').click(function() { _gaq.push(['_link', this.href]); return false; });*/
				jQuery('$linkSelector').click(function() { 
					var newLink = this.href; 
					_gaq.push( function(){
						var tracker = _gaq._getAsyncTracker();
						window.open(tracker._getLinkerUrl(newLink));
					}); 
					return false; 
				});
				/*jQuery('$formSelector').submit(function() { _gaq.push(['_linkByPost', this]); });*/
				jQuery('$formSelector').submit(function(e) {
					this.method ='get';
					this.target ='_blank';
					var newForm = this;
					var Hotel = '?Hotel=' + newForm.elements['hotel'].value;
					var Chain = '&Chain=' + newForm.elements['chain'].value;
					var Promo = '&promo=' + newForm.elements['promo'].value;
					_gaq.push( function(){
						var tracker = _gaq._getAsyncTracker();
						window.open(tracker._getLinkerUrl(newForm.action+Hotel+Chain+Promo));
					}); 
					if (e.preventDefault) {
    						e.preventDefault();
					} else {
   						e.returnValue = false;
					}
					return false; 
				});
			});
		</script>
EOF;
	}
	?>


</head>

<body <?php echo 'class="tmpl_component ' . ($frontPage ? 'front-page' : "other-page $urlClass") . ' ' . $locationClass . '"'; /* If the page is the front page it will have a class of "front-page", otherwise it will have a class of based on the address of the page and a class of "other-page". */ ?>>
	<table id="content-columns">
		<tr>
			<td id="content-center">
				<div id="content-center-sub" class="content-div">
					<jdoc:include type="message" />
					<jdoc:include type="component" />
				</div>
			</td>
		</tr>
	</table>
</body>

</html>