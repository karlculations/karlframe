// Popup Menu Fixes - based on http://www.htmldog.com/articles/suckerfish/dropdowns/ + custom code
sfHover = function() {
	var elms = document.getElementsByTagName("LI");
	for (var i=0; i<elms.length; i++) {
		elms[i].onmouseover=function() { this.className+=" menu-hover"; }
		elms[i].onmouseout=function() { this.className=this.className.replace(new RegExp(" menu-hover\\b"), ""); }
		if (elms[i].parentNode.className.indexOf('menu')==-1 && elms[i].parentNode.parentNode && elms[i].parentNode.parentNode.parentNode && elms[i].parentNode.parentNode.parentNode.className.indexOf('popup-menu')!=-1) {
			var links = elms[i].getElementsByTagName('A');
			if (links.length==1) { links[0].style.width = (elms[i].offsetWidth-24) + 'px'; } // Forces full-width links since display:block doesn't seem to work properly.
		}
	}
}
