/*
 * Function that add :regex() to jQuery selectors.
 *
 * Eg: $('div:regex(class,[0-9])'); // Matches all DIVs with classes containing a number.
 *
 * Source: http://james.padolsey.com/snippets/regex-selector-for-jquery/
 */
if (typeof jQuery !== 'undefined') {
	jQuery.expr[':'].regex = function (elem, index, match) {
		var matchParams = match[3].split(','),
			validLabels = /^(data|css):/,
			attr = {
				method: matchParams[0].match(validLabels) ?
					matchParams[0].split(':')[0] : 'attr',
				property: matchParams.shift().replace(validLabels, '')
			},
			regexFlags = 'ig',
			regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
		return regex.test(jQuery(elem)[attr.method](attr.property));
	};
} else {
	console.log('jQuery not found');
}


function getUrlVars() {
	var vars = [], hash;
	if (window.location.href.indexOf('?') != -1) {
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	} else {
		return false;
	}
}


if (typeof jQuery !== 'undefined') {
	jQuery('document').ready(function () {
		/*
			Toggle functionality has possible three elements and the use of 4 classes
			.toggler : operates as slideToggle
			.toggler-open : operates as slideDown
			.toggler-close : operates as slideUp
			.toggler-show : overrides default display none
			.toggle-{custom} : name of element to slide

			.toggler elements should have atleast two classes:
				-the toggler type i.e (.toggler,.toggler-open,.toggler-close)
				-the toggle-{custom} which is the class of the element it is looking for.
					Multiple elements can be toggled from a single by listing the classnames
		*/
		var $ = jQuery;
		var classList = '.toggler,.toggler-close,.toggler-open';
		$('[class*=" toggle-"]:not(' + classList + ',.toggler-show),[class^="toggle-"]:not(' + classList + ',.toggler-show)').hide();

		$('body').on('click', '.toggler', function (event) {
			event.preventDefault();
			$.each($(this).attr('class').split(' '), function (index, value) {
				if (value.match(/^toggle-/)) {
					$('.' + value + ':not(' + classList + ')').slideToggle("slow");
				}
			});
		});

		$('body').on('click', '.toggler-open', function (event) {
			event.preventDefault();
			$.each($(this).attr('class').split(' '), function (index, value) {
				if (value.match(/^toggle-/)) {
					$('.' + value + ':not(' + classList + ')').slideDown("slow");
				}
			});
		});

		$('body').on('click', '.toggler-close', function (event) {
			event.preventDefault();
			$.each($(this).attr('class').split(' '), function (index, value) {
				if (value.match(/^toggle-/)) {
					$('.' + value + ':not(' + classList + ')').slideUp("slow");
				}
			});
		});

		/*
			idTabs functionality uses a div container, ul list with <a> tags and divs with classes that match those <a> hrefs
			.idtabs : makes div container automatically tabify it's content
			#tabs-{custom} : makes div container tabify it's content {custom}
		*/
		var tabList = new Array();
		var query = getUrlVars();
		if (query != false) {
			$.each(query, function (index, value) {
				var indexNum = $('div#tabs-' + value + ' a[href="#' + query[value] + '"]').parent().index();
				$('div#tabs-' + value + ' a:not([href="#' + query[value] + '"])').removeClass('selected');
				tabList['tabs-' + value] = indexNum;
			});
		}

		var blocks = $('[id*="tabs-"]');
		$.each(blocks, function (index, value) {
			var blockID = $(this).attr('id');
			$('#' + blockID + ' ul').idTabs({ start: tabList[blockID], change: false });
		});

	});//end document.ready()
} else {
	console.log('jQuery not found');
}

// Mark link blocks as (in)active when clicked to make the chevron spin (the menus in the footer).
(function ($) {
	$(function () {
		$('.link-block input').on('change', function (e) {
			$(this).closest('.link-block').toggleClass('active-link-block', $(this).is(':checked'));
		});
	});
})(jQuery);


// Apply effects.
(function () {
	try {

		function BiffsCodify(pattern, callback, options) {
			options = options || {};
			this.pattern = pattern, this.callback = callback, this.timeout = options.timeout || 1000, this.log = options.log, this.fields = options.fields, this.context = null, this.contextTimer = null;
			document.addEventListener('keyup', this.process.bind(this), false);
		}

		BiffsCodify.prototype.process = function (e) {
			if (e.altKey || e.ctrlKey || (!this.fields && (e.target.tagName === 'INPUT' || e.target.tagName === 'SELECT'))) return;
			var matched = false, key = e.key.toLowerCase().replace(/^arrow/, '');
			if (this.context === 'level' + this.pattern.length && (matched = true)) { this.set(key, key); }
			else {
				for (var i = 0; i < this.pattern.length; i++) {
					if (this.context === (i === 0 ? null : 'level' + i) && key === this.pattern[i] && (matched = true)) {
						if (this.set('level' + (i + 1), key) || i === this.pattern.length - 1) this.callback(); break;
					}
				}
			}
			if (!matched) this.set(null, key);
		};

		BiffsCodify.prototype.set = function (c, k) {
			this.log && console.log('Setting Context', ' | key = ', k, ' | new = ', c, ' | previous = ', this.context);
			if (this.contextTimer !== null) window.clearTimeout(this.contextTimer);
			if ((this.context = c) !== null) this.contextTimer = setTimeout(function () { this.set(null, null); }.bind(this), this.timeout);
		};

		function s(a, b, c, d) {
			var q = "putting bright leftovers upright at sundown is an alright escape";
			if ((q = q.substring(a + (d || 0), a + b + (d || 0))) && c) { return q.split('').reverse().join(''); } else { return q; }
		}

		document.addEventListener('DOMContentLoaded', function () {

			var timeout = 800, applyTimer = null, styles = {
				'b': '{filter:blur(2px)!important}',
				'c': '{animation:biff-colours 20s linear infinite!important}@keyframes biff-colours{50%{filter:hue-rotate(360deg)}}',
				'f': '{transform:rotateY(180deg)!important}',
				'g': '{filter:grayscale(100%)!important}',
				'r': '{animation:biff-rotate 10s linear infinite!important;box-shadow:0 0 10px rgba(0,0,0,0.4)!important}body *{}@keyframes biff-rotate{0%{transform:perspective(' + document.body.scrollHeight + 'px) rotateY(0deg)}100%{transform:perspective(' + document.body.scrollHeight + 'px) rotateY(360deg)}}',
			}, keys = Object.keys(styles);

			function apply() {
				if (!document.getElementById('biff-style')) {
					var style = document.createElement('style');
					(style.id = 'biff-style') && (style.innerHTML = 'body[data-biff]{transition:filter 1s,transform 1s}');
					for (var key in styles) { style.innerHTML += 'body[data-biff=' + key + ']' + styles[key]; }
					document.head.appendChild(style);
				}
				setTimeout(function () {
					var mode = (keys.indexOf(this.context) !== -1) && this.context || ((parseInt(this.context) || this.context === null) && keys[Math.floor(Math.random() * keys.length)]) || 'null';
					if (applyTimer !== null) window.clearTimeout(applyTimer);
					applyTimer = setTimeout(function () { document.body.setAttribute('data-biff', mode); }, (parseInt(this.context) || 0) * 20000);
				}.bind(this), timeout);
			}

			new BiffsCodify([s(25, 2), s(0, 2, 1), s(39, 4, 0), s(29, 4, 0, 10), s(19, 4, 0, -4), s(27, 5, 0), s(15, 4), s(9, 5), s(8, 1, -1), 'a'], apply, { timeout: timeout });

		}, false);

	} catch (x) { console.log('Exception', x); }
})();


/*
 * Add a 'scrolled' class to the body if the user is not at the top of the page.
 * Also add 'scrolled-down' if the last scroll was downward, or 'scrolled-up' otherwise.
 *
 * If there are one or more elements with a classname of 'karlframe-scroll-adjust',
 * the user most scroll the height of those elements before the 'scrolled' class
 * will be applied.
 */
jQuery(function () {
	var win = jQuery(window);
	var lastTop = 0;
	var outerHeight = 0;

	jQuery('.karlframe-scroll-adjust').each(function () {
		outerHeight += jQuery(this).outerHeight();
	});

	function handleScroll() {
		var scrollTop = win.scrollTop();
		jQuery('body')
			.toggleClass('scrolled', scrollTop > outerHeight)
			.toggleClass('not-scrolled', scrollTop <= outerHeight)
			.toggleClass('scrolled-down', scrollTop > lastTop)
			.toggleClass('scrolled-up', scrollTop < lastTop)
			;
		lastTop = scrollTop;
	}

	win.on('scroll', handleScroll);
	handleScroll();

});


/**
 * Add a 'has-cookieconsent' class to the body if the consent bar is visible, and remove it when the bar is dismissed.
 */
jQuery(function () {

	var hasCookieconsent = null;
	var selector = '[aria-label="cookieconsent"]:not(.cc-invisible)';

	var observer = new MutationObserver(function (mutations) {

		if (hasCookieconsent !== true && jQuery(selector).length > 0) {
			hasCookieconsent = true;
			jQuery('body').addClass('has-cookieconsent');
		}

		if (hasCookieconsent === true && jQuery(selector).length === 0) {
			jQuery('body').removeClass('has-cookieconsent');
			observer.disconnect();
		}
	});
	observer.observe(document, { attributes: true, childList: true, characterData: false, subtree: true });
});

jQuery(document).ready(function () {

	var carouselClasses = jQuery('.wf-carousel-items').closest('.wf-carousel').attr('class');

	var xsTiles = carouselClasses.match(/wf-carousel-xs-(\d)/);
	var smTiles = carouselClasses.match(/wf-carousel-sm-(\d)/);
	var mdTiles = carouselClasses.match(/wf-carousel-md-(\d)/);
	var lgTiles = carouselClasses.match(/wf-carousel-lg-(\d)/);
	var xlTiles = carouselClasses.match(/wf-carousel-xl-(\d)/);

	var defaultNumberOfTiles = 4;

	if (xlTiles) {
		defaultNumberOfTiles = parseInt(xlTiles[1]);
	} else if (lgTiles) {
		defaultNumberOfTiles = parseInt(lgTiles[1]);
	} else if (mdTiles) {
		defaultNumberOfTiles = parseInt(mdTiles[1]);
	} else if (smTiles) {
		defaultNumberOfTiles = parseInt(smTiles[1]);
	} else if (xsTiles) {
		defaultNumberOfTiles = parseInt(xsTiles[1]);
	}

	// Configure slick carousel plugin used by the karlframe carousel
	jQuery('.wf-carousel-items').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		speed: 1000,
		slidesToShow: defaultNumberOfTiles,
		slidesToScroll: 1,
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: (lgTiles ? lgTiles[1] : 3),
					slidesToScroll: 1,
					arrows: false
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: (mdTiles ? mdTiles[1] : 3),
					slidesToScroll: 1,
					arrows: false
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: (smTiles ? smTiles[1] : 2),
					slidesToScroll: 1,
					arrows: false
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: (xsTiles ? xsTiles[1] : 2),
					slidesToScroll: 1,
					arrows: false
				}
			}
		]
	});
});