<?php

jimport('joomla.application.module.helper');


/**
 * Stores information on the KarlFrame Resources
 */
class KarlFrameResource
{

	/** @var integer  The value of an unknown library url. */
	const UNKNOWN = 0;

	/** @var integer  The value of a style library url. */
	const STYLE = 1;

	/** @var integer  The value of a javascript library url. */
	const SCRIPT = 2;

	/** @var integer  The resource type, one of: KarlFrameResource::UNKNOWN, KarlFrameResource::STYLE, KarlFrameResource::SCRIPT.*/
	public $type;

	/** @var string  The full url of resource on of: //cdnjs.cloudflare.com/ajax/libs/modernizr/%s/modernizr.min.js,
	 * //cdnjs.cloudflare.com/ajax/libs/cookieconsent2/%s/cookieconsent.min.css.
	 */
	public $url;

	/** @var string  Whether the resource should be deferred.*/
	public $defer = false;


	/** 
	 * Contructor 
	 * @param string 	$type 	The type of resource.
	 * @param string 	$url 	The url of the resource.
	 * @param boolean 	$defer 	Whether the resource should be deferred.
	 */
	public function __construct($type, $url, $defer = false)
	{
		$this->type = $type;
		$this->url = $url;
		$this->defer = $defer;
	}
}
