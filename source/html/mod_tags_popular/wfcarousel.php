<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_tags_popular
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php'); ?>
<div class="tagspopular<?php echo $moduleclass_sfx; ?> wf-carousel container-fluid">
<?php if (!count($list)) : ?>
	<div class="alert alert-no-items"><?php echo JText::_('MOD_TAGS_POPULAR_NO_ITEMS_FOUND'); ?></div>
<?php else : ?>
	<?php

		if (!function_exists('getArticlesWithTag')) {
			function getArticlesWithTag($tagId) {
				// Get all articles by a particular tag. This function works for articles by child tag as well.
				$db = \JFactory::getDbo();
				$prefix = $db->getPrefix();
				$query = $db -> getQuery(true);
				
				$query -> SELECT(array('articles.*', 'tagmap.content_item_id', 'tagmap.tag_id'));
				$query -> FROM($db->quoteName('#__contentitem_tag_map', 'tagmap'));
				$query -> JOIN('LEFT', $db->quoteName('#__content', 'articles') . ' ON (' 
							.$db->quoteName('tagmap.content_item_id')
							. ' = '
							. $db->quoteName('articles.id') . ')');
				
				$query -> WHERE(
							$db->quoteName('tagmap.tag_id') . ' = ' . $tagId
							. ' AND ' . $db->quoteName('tagmap.type_alias') . ' = "com_content.article"'
							. ' AND ' . $db->quoteName('articles.state') . ' = 1');

				$db->setQuery($query);
				$results = $db->loadObjectList();

				return $results;
			}
		}

		if (!function_exists('getCategoriesWithTag')) {
			function getCategoriesWithTag($tagId) {
				// Get all categories by a particular tag. This function works for child categories as well.
				$db = \JFactory::getDbo();
				$prefix = $db->getPrefix();
				$query = $db -> getQuery(true);
				
				$query -> SELECT(array('categories.*', 'tagmap.content_item_id', 'tagmap.tag_id'));
				$query -> FROM($db->quoteName('#__contentitem_tag_map', 'tagmap'));
				$query -> JOIN('LEFT', $db->quoteName('#__categories', 'categories') . ' ON (' 
							.$db->quoteName('tagmap.content_item_id')
							. ' = '
							. $db->quoteName('categories.id') . ')');
				
				$query -> WHERE(
							$db->quoteName('tagmap.tag_id') . ' = ' . $tagId
							. ' AND ' . $db->quoteName('tagmap.type_alias') . ' = "com_content.category"'
							. ' AND ' . $db->quoteName('categories.published') . ' = 1');

				$db->setQuery($query);
				$results = $db->loadObjectList();

				return $results;
			}
		}
	?>
	<?php foreach ($list as $tag) : ?>

	<?php
		$articles = getArticlesWithTag($tag->tag_id);
		$categories = getCategoriesWithTag($tag->tag_id);

		$contentItems = array_merge($articles, $categories); // merge articles and categories items
		shuffle($contentItems); // randomize the content items so they don't always display in the same order
	?>
	<?php endforeach; ?>

	<?php if ($module->showtitle) : ?>
		<div class="wf-carousel-intro-text">
			<div class="row">
				<div class="col-12">
					<h3> <?= $module->title ?> </h3>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="wf-carousel-items">
		<?php foreach ($contentItems as $item) : ?>
			<?php
				// Get the values of the text, image and link for the carousel from the article or category
				if (array_key_exists('images', $item)) { // $item is an article
					$itemImages = json_decode($item->images);
					$slug    = $item->id . ':' . $item->alias;
					$link = JRoute::_(ContentHelperRoute::getArticleRoute($slug, $item->catid, $item->language));
				} else { // $item is a category
					$imgParams = json_decode($item->params);
					$images = (object) [
						'image_intro' => $imgParams->image,
						'image_intro_alt' => $imgParams->image_alt
					];
					$itemImages = $images;

					$link  = JRoute::_(ContentHelperRoute::getCategoryRoute($item->id));
				}
			?>
			<div class="wf-carousel-item">
				<a href="<?= $link ?>" title="<?= $item->title ?>">
					<img src="<?= $itemImages->image_intro ?>" class="background-image" />
					<div class="overlay">
						<div class="caption"><?= $itemImages->image_intro_alt ?></div>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
</div>
