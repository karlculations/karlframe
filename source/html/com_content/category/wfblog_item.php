<?php

/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;

// Get Joomla custom fileds
$h = new Harvester($this->item);

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

$bootstrapColumns =  KarlFrameBootstrap::getBootstrapColumnClasses();
echo '<div class="' . $bootstrapColumns . ' wf-tile wf-category-blog-tile">';

?>
<?php if (
	$this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != JFactory::getDbo()->getNullDate())
) : ?>
	<div class="system-unpublished">
	<?php endif; ?>

	<?php
	// Check that custom fields for tiles are defined and have values 
	// otherwise use the content from Joomla "Images and Links" params for the article
	$hasCustomFields = true; // Used to determine where to get the content for the tiles

	try {
		foreach (['wf-tile-thumbnail', 'wf-tile-caption-1', 'wf-tile-caption-2'] as $field) {
			if (!is_string($h->get($field)) || $h->get($field) == '') {
				$hasCustomFields = false;
			}
		}
	} catch (Exception $e) {
		$hasCustomFields = false;
	}

	if ($hasCustomFields) {
		// The is_array is used for very rare cases where there are duplicate custom field names stored
		$thumbnail = is_array($h->get('wf-tile-thumbnail')) ? $h->get('wf-tile-thumbnail')[0] : $h->get('wf-tile-thumbnail');
		$captionOne = is_array($h->get('wf-tile-caption-1')) ? $h->get('wf-tile-caption-1')[0] : $h->get('wf-tile-caption-1');
		$captionTwo = is_array($h->get('wf-tile-caption-2')) ? $h->get('wf-tile-caption-2')[0] : $h->get('wf-tile-caption-2');
	} else {
		// Values from Joomla images and links 
		$itemImgIntro = json_decode($this->item->images);
		$thumbnail = $itemImgIntro->image_intro;
		$captionOne = $itemImgIntro->image_intro_caption;
		$captionTwo = $itemImgIntro->image_intro_alt;
	}
	?>

	<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language)); ?>">
		<img class="wf-tile-thumbnail" src="<?= $thumbnail; ?>" />
		<div class="wf-tile-caption-1"><?= $captionOne ?></div>
		<div class="wf-tile-caption-2"><?= !empty($captionTwo) ? $captionTwo : $this->item->title; ?></div>
	</a>

	<?php if ($canEdit || $params->get('show_print_icon') || $params->get('show_email_icon')) : ?>
		<?php echo JLayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item, 'print' => false)); ?>
	<?php endif; ?>

	<?php if ($this->params->get('show_cat_tags') && !empty($this->item->tags->itemTags)) : ?>
		<?php //echo JLayoutHelper::render('joomla.content.tags', $this->item->tags->itemTags); 
		?>
	<?php endif; ?>

	<?php
	?>
	<?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
		|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author')); ?>

	<?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
		<?php //echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); 
		?>
	<?php endif; ?>

	<?php //echo JLayoutHelper::render('joomla.content.intro_image', $this->item); 
	?>


	<?php if (!$params->get('show_intro')) : ?>
		<?php echo $this->item->event->afterDisplayTitle; ?>
	<?php endif; ?>
	<?php echo $this->item->event->beforeDisplayContent; ?> <?php //echo $this->item->introtext; 
															?>

	<?php if ($useDefList && ($info == 1 || $info == 2)) : ?>
		<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
	<?php endif; ?>

	<?php if ($params->get('show_readmore') && $this->item->readmore) :
		if ($params->get('access-view')) :
			$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language));
		else :
			$menu = JFactory::getApplication()->getMenu();
			$active = $menu->getActive();
			$itemId = $active->id;
			$link = new JUri(JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false));
			$link->setVar('return', base64_encode(JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language), false)));
		endif; ?>

		<?php echo JLayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>

	<?php endif; ?>

	<?php if (
		$this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
		|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != JFactory::getDbo()->getNullDate())
	) : ?>
	</div>
<?php endif; ?>

<?php echo $this->item->event->afterDisplayContent;
echo '</div>';


/* Prevent the grid from breaking. */
KarlFrameBootstrap::autoClearfix($bootstrapColumns);

?>