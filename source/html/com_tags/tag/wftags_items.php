<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.core');

JLoader::register('KarlFrameBootstrap', 'templates/karlframe/KarlFrameBootstrap.php');
KarlFrameBootstrap::resetClearfixIndex();

// Get the user object.
$user = JFactory::getUser();

// Check if user is allowed to add/edit based on tags permissions.
$canEdit = $user->authorise('core.edit', 'com_tags');
$canCreate = $user->authorise('core.create', 'com_tags');
$canEditState = $user->authorise('core.edit.state', 'com_tags');

$items = $this->items;

$count = 1; // To track the amount of items output, so we can add clearfixes
$bootstrapColumns = KarlFrameBootstrap::getBootstrapColumnClasses();

?>
<div class="container-fluid wf-tag-tiles">
	<div class="row">
		<?php
		foreach ($items as $i => $item) :
			$h = new Harvester($item);

			// Check that custom fields for tiles are defined and have values 
			// otherwise use the content from Joomla "Images and Links" params for the article
			$hasCustomFields = true; // Used to determine where to get the content for the tiles

			try {
				foreach (['wf-tile-thumbnail', 'wf-tile-caption-1', 'wf-tile-caption-2'] as $field) {
					if (!is_string($h->get($field)) || $h->get($field) == '') {
						$hasCustomFields = false;
					}
				}
			} catch (Exception $e) {
				$hasCustomFields = false;
			}

			if ($hasCustomFields) {
				// The is_array is used for very rare cases where there are duplicate custom field names stored
				$thumbnail = is_array($h->get('wf-tile-thumbnail')) ? $h->get('wf-tile-thumbnail')[0] : $h->get('wf-tile-thumbnail');
				$captionOne = is_array($h->get('wf-tile-caption-1')) ? $h->get('wf-tile-caption-1')[0] : $h->get('wf-tile-caption-1');
				$captionTwo = is_array($h->get('wf-tile-caption-2')) ? $h->get('wf-tile-caption-2')[0] : $h->get('wf-tile-caption-2');
			} else {
				// Values from Joomla images and links 
				$itemImgIntro = json_decode($item->core_images);
				$thumbnail = $itemImgIntro->image_intro;
				$captionOne = $itemImgIntro->image_intro_caption;
				$captionTwo = $itemImgIntro->image_intro_alt;
			}
		?>
			<div class="<?= $bootstrapColumns; ?> wf-tile wf-tag-tile">
				<a href="<?php echo JRoute::_(TagsHelperRoute::getItemRoute($item->content_item_id, $item->core_alias, $item->core_catid, $item->core_language, $item->type_alias, $item->router)); ?>">
					<img class="wf-tile-thumbnail img-responsive" src="<?= $thumbnail; ?>" />
					<div class="wf-tile-caption-1"><?= $captionOne; ?></div>
					<div class="wf-tile-caption-2"> <?= !empty($captionTwo) ? $captionTwo : $item->core_title; ?></div>
				</a>
			</div>
			<?php KarlFrameBootstrap::autoClearfix($bootstrapColumns, $count++); // Prevent the grid from breaking. 
			?>
		<?php endforeach; ?>
	</div>
</div>