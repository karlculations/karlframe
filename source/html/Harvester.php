<?php

defined('_JEXEC') or die;

JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');	

/**
 * Assists with using Joomla's custom fields.
 *
 * Sample Usage 1:
 * ```
 * JLoader::register('Harvester', 'templates/karlframe/html/Harvester.php');
 * $h = new Harvester($item);
 * echo $h->get('field-1');
 * echo $h->get('field-2');
 * ```
 *
 * Sample Usage 2:
 * ```
 * JLoader::register('Harvester', 'templates/karlframe/html/Harvester.php');
 * echo Harvester::getFieldValue($item->jcfields, 'field-1');
 * echo Harvester::getFieldValue($item->jcfields, 'field-2');
 * ```
 */
class Harvester extends FieldsHelper {

	/**
	 * The values of the fields.
	 * The key is the field's name; the value is the field's raw value.
	 * @var string[]
	 */
	protected $fields = [];


	/**
	 * Constructor.
	 * @param  mixed    $item     If the item is an object, then Joomla's FieldsHelper will be used. Otherwise, the value is assumed to be an aritlce or category ID, and Harvester directly queries the database to get the fields.
	 * @param  string   $context  The item's context, eg 'com_content.article', 'com_content.categories' or 'com_users.user'.
	 */
	public function __construct($item, $context='com_content.article') {

		if ($context===null) {
			$context='com_content.article';
		}

		$this->fields = [];

		if (is_object($item)) {

			foreach (FieldsHelper::getFields($context, $item, true) as $field) {
				$this->fields[$field->name] = $field->rawvalue;
			}

		} else {

			$db = JFactory::getDbo();
			
			$query = $db->getQuery(true)
	            ->select($db->quoteName(['fields.name', 'values.value']))
	            ->from($db->quoteName('#__fields_values', 'values'))
	            ->join('INNER', $db->quoteName('#__fields', 'fields') . ' ON (' . $db->quoteName('values.field_id') . ' = ' . $db->quoteName('fields.id') . ')')
	            ->where('item_id = ' . $db->Quote($item));
			
			$db->setQuery($query);
			$results = $db->loadObjectList();

			foreach ($results as $result) {
				$this->fields[$result->name] = $result->value;
			}

		}
	}


	/**
	 * Returns the value of the specified field for the item stored in `$this->item`.
	 * @throws  UnexpectedValueException if the field doesn't exist or doesn't have a raw value.
	 * @param   string  $fieldName  The requested field name.
	 * @param   bool  	$optional  	Is the requested field required?
	 * @return  string              The value of the field.
	 */
	public function get($fieldName, $optional=false) {
		if (array_key_exists($fieldName, $this->fields)) {
			return $this->fields[$fieldName];
		}
		
		if ($optional) {
			return false;
		}

		throw new UnexpectedValueException(sprintf(
			'Harvester could not find a field called "%s". Valid fields are: %s.',
			$fieldName,
			join(', ', array_keys($this->fields))
		));
	}


	/**
	 * Finds the field with the specific name in the specified array of fields.
	 * @throws  UnexpectedValueException if the field doesn't exist.
	 * @param   object[]  $fields     The list of fields.
	 * @param   string    $fieldName  The requested field name.
	 * @return  object                The matching field.
	 */
	public static function getField($fields, $fieldName){

		foreach ($fields as $field) {
			if ($field->name === $fieldName) {
				return $field;
			}
		}
		
		throw new UnexpectedValueException(sprintf('Harvester could not find a field called "%s".', $fieldName));
	}


	/**
	 * Returns the raw value of the specified field.
	 * @throws  UnexpectedValueException if the field doesn't have a raw value.
	 * @param   string  $field  The field.
	 * @return  string          The raw value.
	 */
	public static function getFieldValue($field){
		
		if ($field) {
			return $field->rawvalue;
		} else {
			throw new UnexpectedValueException(sprintf('Harvester could not find a raw value for the field "%s".', $field->name));
		}
	}
}
