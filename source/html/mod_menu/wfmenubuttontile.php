<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$activeTemplate   = JFactory::getApplication()->getTemplate(true);
$bootstrapVersion = $activeTemplate->params->get('bootstrap-version', '3');

$count = 1; // To track the amount of items output, so we can add clearfixes
$bootstrapColumns = KarlFrameBootstrap::getBootstrapColumnClasses();

?>
<div class="container-fluid wf-menu-button-tiles">
	<div class="row">
		<?php foreach ($list as $i => &$item) : ?>
			<div class="<?= $bootstrapColumns ?> wf-tile wf-menu-button-tile">
				<a href="<?= ($item->type == 'alias') ? $item->flink : $item->link ?>" title="<?= $item->title ?>" <?= ($item->anchor_css) ? ' class="' . $item->anchor_css . '"' : '' ?>>
					<?php if ($item->menu_image) : ?>
						<img src="<?= $item->menu_image ?>" class="wf-tile-thumbnail" />
					<?php endif; ?>
					<div>
						<div class="wf-tile-caption"><?= $item->title ?></div>
						<div class="btn btn-primary wf-tile-button"><?= $item->anchor_title ?></div>
					</div>
				</a>
			</div>
			<?php KarlFrameBootstrap::autoClearfix($bootstrapColumns, $count++); ?>
		<?php endforeach; ?>
	</div>
</div>