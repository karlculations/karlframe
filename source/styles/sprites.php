<!DOCTYPE html>
<html>
	<head>
		<title>Sprite Tester</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-script-type" content="text/javascript" />
		<meta http-equiv="content-style-type" content="text/css" />
		<meta name="description" content="" />
		<link type="text/css" rel="stylesheet" href="editor.custom.css" />
		<style type="text/css">
			img, .shadowed span { box-shadow:0 0 1px rgba(0,0,0,0.5); }
			img, span { margin:5px; }
		</style>
	</head>
	<body>
		<br />
<?php

$sprites = '';
$testedClasses = Array();
foreach (glob('*custom.css') as $stylesheet) {
	$classnames = Array();
	foreach (file($stylesheet) as $line) {
		if (preg_match('/^\.(sprite-[a-zA-Z0-9-]+)/', $line, $m) && !in_array($m[1], $testedClasses)) {
			$testedClasses[] = $m[1];
			$sprites.= '<span class="' . $m[1] . '" title="' . $m[1] . '">&nbsp;</span>';
			if (strstr($line, '{')) {
				$sprites.= '<br />';
			}
		}
	}
}
$sprites.= '<br /><hr />';

echo $sprites;
echo '<div class="shadowed">' . $sprites . '</div>';

?>
		<script type="text/javascript">
			var processedImages = [];
			for (var i=0, spans=document.getElementsByTagName('span'); i<spans.length; i++) {
				var image = window.getComputedStyle(spans[i])['background-image'];
				if (processedImages.indexOf(image)==-1) {
					processedImages.push(image);
					var src = image.replace(/^url\(|\)$/g, '');
					document.write('<br />' + src + ':<br /><br /><img src="' + src + '" title="' + src + '" /><br /><br /><hr />');
				}
			}
		</script>
	</body>
</html>
