<?php

jimport('joomla.application.module.helper');


/**
 * Stores the Libraries which are inputed in Joomla.
 */
class KarlFrameLibraries
{

	/** @var  KarlFrameResource[]  A collection of javascript URL to be added to the page. */
	public $scripts = [];

	/** @var  KarlFrameResource[]  A collection of stylesheets to be added to the page. */
	public $styles = [];

	/** @var  string  The system path to the folder containing the template, relative to Joomla's templates folder, eg "karlframe". */
	public static $templatePath;

	/** @var  string  The base url of the current site. */
	public static $baseUrl;

	/** @var  integer  The cache version that the site is on.  */
	public static $cacheVersion;

	/** @var  KarlFrameLibrary[]  A collection of defined resources. */
	public static $shortcuts;

	/** @var  boolean  Tracks whether the joomla's version of jquery is being used. */
	public $jqueryFromJoomla = false;

	/** @var  boolean  Tracks whether the joomla's version of bootstrap is being used. */
	public $bootstrapFromJoomla = false;


	/**
	 * Constructor.
	 * @param  string  $scripts  List of libraries to include, one per line.
	 * @throws UnexpectedValueException If it cannot be determined whether a library is a script or a style.
	 */
	public function __construct($scripts)
	{

		foreach (preg_split('/(\r?\n\r?)+/', $scripts) as $library) {

			// CDN Libraries using shortcuts.
			if (preg_match('/^(\S+)\s([\d.]+|joomla)(?:\s(defer))?$/', $library, $m) && array_key_exists($m[1], static::$shortcuts)) {

				foreach (static::$shortcuts[$m[1]] as $resource) {

					if ($m[2] === 'joomla') {

						if ($m[1] === 'jquery') {
							$this->jqueryFromJoomla = true;
						}

						if ($m[1] === 'bootstrap') {
							$this->bootstrapFromJoomla = true;
						}
					} else {

						$resource->url = sprintf($resource->url, $m[2]);

						if ($resource->type === KarlFrameResource::SCRIPT) {
							if (array_key_exists(3, $m) && $m[3] == 'defer') {
								$resource->defer = true;
							}
							$this->scripts[] = $resource;
						} else if ($resource->type === KarlFrameResource::STYLE) {
							$this->styles[] = $resource;
						}
					}
				}
			} else {

				// Determine resource type.
				$type = static::parseType($library);
				if ($type === KarlFrameResource::UNKNOWN) {
					throw new UnexpectedValueException('Unknown resource type: ' . $library);
				}

				// Full URLs.
				if (strstr($library, '//')) {
					if ($type === KarlFrameResource::STYLE) {
						$this->styles[] = new KarlFrameResource($type, $library);
					} else {
						$this->scripts[] = new KarlFrameResource($type, $library);
					}
				}

				// Stylesheets in the 'styles' folder.
				else if ($type === KarlFrameResource::STYLE) {
					$path = '/templates/' . static::$templatePath . '/styles/' . $library;
					if (is_file(getcwd() . '/' . $path)) {
						$this->styles[] = new KarlFrameResource($type, static::$baseUrl . $path . '?' . static::$cacheVersion);
					}
				}

				// Scripts in the 'scripts' folder.
				else if ($type === KarlFrameResource::SCRIPT) {
					$path = '/templates/' . static::$templatePath . '/scripts/' . $library;
					if (is_file(getcwd() . '/' . $path)) {
						$this->scripts[] = new KarlFrameResource($type, static::$baseUrl . $path . '?' . static::$cacheVersion);
					}
				}
			}
		}
	}


	/**
	 * Determines the resource type by examining the file extension in the URL.
	 * @param   string  $url  The URL to examine.
	 * @return  integer  The parsed type, as one of the class constants defined in KarlFrameResource.
	 */
	public static function parseType($url)
	{
		if (preg_match('/\.(css|less)(\?[A-z\d]+)?$/', $url)) {
			return KarlFrameResource::STYLE;
		} else if (preg_match('/\.js(\?[A-z\d]+)?$/', $url)) {
			return KarlFrameResource::SCRIPT;
		}
		return KarlFrameResource::UNKNOWN;
	}
}


// Define URLs for scripts/styles from CDNs.
KarlFrameLibraries::$shortcuts = array(
	'bootstrap'          => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/%s/js/bootstrap.min.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/%s/css/bootstrap.min.css'),
	],
	'bootstrap-select'   => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/%s/js/bootstrap-select.min.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/%s/css/bootstrap-select.min.css'),
	],
	'cookie-consent'     => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/%s/cookieconsent.min.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/%s/cookieconsent.min.css'),
	],
	'jquery'			 => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/jquery/%s/jquery.min.js')],
	'jquery-balancetext' => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/balance-text/%s/jquery.balancetext.min.js')],
	'jquery-color'       => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/jquery-color/%s/jquery.color.min.js')],
	'jquery-cookie'      => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/%s/jquery.cookie.min.js')],
	'jquery-slicknav'    => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/SlickNav/%s/jquery.slicknav.min.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/SlickNav/%s/slicknav.min.css'),
	],
	'jquery-ui'          => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/jqueryui/%s/jquery-ui.min.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/jqueryui/%s/jquery-ui.min.css'),
	],
	'less'               => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/less.js/%s/less.min.js')],
	'modernizr'          => [new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/modernizr/%s/modernizr.min.js')],
	'date-range-picker'    => [
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/%s/moment.min.js'),
		new KarlFrameResource(KarlFrameResource::SCRIPT, '//cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/%s/daterangepicker.js'),
		new KarlFrameResource(KarlFrameResource::STYLE, '//cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/%s/daterangepicker.css'),
	],
);
