<?php

jimport('joomla.application.module.helper');


/**
 * Assists with Bootstrap implementations.
 */
class KarlFrameBootstrap
{

	/**
	 * Used by `autoClearfix()` to track the number of items that have been displayed.
	 * @var integer
	 */
	private static $clearfixIndex = 0;


	/**
	 * Returns all the bootstrap related classnames for containers, as specified in KarlFrame's params.
	 * 
	 * Sample usage:
	 * ```php
	 * $app      = JFactory::getApplication();
	 * $template = $app->getTemplate(true);
	 * $classes  = KarlFrameBootstrap::getBootstrapClasses($template->params, false, true);
	 * ```
	 * 
	 * @param   Registry  $params  The standard Joomla template parameters.
	 * @return  string[]           An associate array with the container ID as the key and classname as the value.
	 */
	public static function getBootstrapClasses(Joomla\Registry\Registry $params, $forceLeft = false, $forceRight = false)
	{

		$containers = array(
			'body-header',
			'body-header-1', 'body-header-2', 'body-header-3',
			'page-header-1', 'page-header-2', 'page-header-3',
			'page',
			'content-columns-sub',
			'content-left', 'content-center', 'content-right',
			'content-top', 'content-component', 'content-bottom',
			'page-footer-1', 'page-footer-2', 'page-footer-3',
			'body-footer',
			'body-footer-1', 'body-footer-2', 'body-footer-3'
		);

		// Get raw class names.
		// These may contain asterisks as wildcards.
		$classes = [];
		foreach ($containers as $value) {
			$classes[$value] = $params->get('classes-' . $value, '');
		}

		// Determine column visibility.
		$hasLeftColumn = $forceLeft || count(JModuleHelper::getModules('content-left')) > 0;
		$hasRightColumn = $forceRight || count(JModuleHelper::getModules('content-right')) > 0;

		// Resolve wildcards.
		foreach (['Left', 'Center', 'Right'] as $column) {
			static::evaluateBootstrapClass($classes, $column, $hasLeftColumn, $hasRightColumn);
		}

		return $classes;
	}


	/**
	 * Evaluates wildcards in Bootstrap classes.
	 *
	 * Currently this only supports content-left, content-center and content-right.
	 *
	 * Eg, if:
	 * - content-center has col-md-*
	 * - content-right has col-md-4
	 *
	 * Then content-center will be evaluated to col-md-8 because 12 - 4 = 8.
	 * But if content-right was not being displayed, content-center would be evaluated to col-md-12 (full-width).
	 * 
	 * @throws  UnexpectedValueException   If an content col classes combination is specifed. e.g content-center has "col-md-*" and content-right has "col-md-12", content center would evaluate to "col-md-0" which isn't valid or useable.
	 * @param   string[]  &$classes        The list of all bootstrap classes. See `getBootstrapClasses()`.
	 * @param   string    $column          The column position which is being evaluated, eg 'left';
	 * @param   boolean   $hasLeftColumn   Whether the left column is visible.
	 * @param   boolean   $hasRightColumn  Whether the right column is visible.
	 */
	public static function evaluateBootstrapClass(array &$classes, $column, $hasLeftColumn, $hasRightColumn)
	{

		$bootstrapVersion = static::getKarlFrameBootstrapVersion();
		$columns = ['Left', 'Center', 'Right'];

		$activeColumn = 'content-' . strtolower($column);

		if (strstr($classes[$activeColumn], '*')) {
			$newClasses = array();
			foreach (preg_split('/\s+/', $classes[$activeColumn]) as $class) {

				if (preg_match('/(col-([a-z][a-z])-(?:\w+-)?)\*/', $class, $m) || (preg_match('/(col-)\*/', $class, $m) && $bootstrapVersion === '4')) {

					$classPrefx = $m[1]; // eg "col-sm" or "col-md-offset".
					$classSize = $m[2]; // eg 'sm' or 'md'.

					$total = 12;

					$otherColumns = $columns;
					if (($key = array_search($column, $otherColumns)) !== false) {
						unset($otherColumns[$key]);
					}

					foreach ($otherColumns as $otherColumn) {
						$var = 'has' . $otherColumn . 'Column';
						if ((!isset($$var) || $$var) && preg_match('/' . $classPrefx . '(\d+)/', $classes['content-' . strtolower($otherColumn)], $n)) {
							$total -= (int) $n[1];
						}
					}

					if ($total > 1) {
						$class = str_replace('*', $total, $class);
					} else {
						$message = sprintf('%s* specified for %s but content %s classes specified as full width. Correct content classes in KarlFrame\'s bootstrap options and try again.', $classPrefx, $activeColumn, strtolower(join(' / ', $otherColumns)));
						throw new UnexpectedValueException($message);
					}
				}
				$newClasses[] = $class;
			}
			$classes[$activeColumn] = join(' ', $newClasses);
		}
	}


	/**
	 * Returns the number of Bootstap columns that would be displayed for the specified size.
	 *
	 * A Bootstrap column is a column in Bootstrap's grid system.
	 * For example, full-width content spans 12 columns.
	 *
	 * For example:
	 * ```php
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-sm-6 col-md-4', 'xs'); // 12
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-sm-6 col-md-4', 'sm'); // 6
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-sm-6 col-md-4', 'md'); // 4
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-sm-6 col-md-4', 'lg'); // 4
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-sm-6 col-md-4', 'xl'); // 4
	 * 
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'xs'); // 6
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'sm'); // 4
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'md'); // 3
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'lg'); // 2
	 * KarlFrameBootstrap::getBootstrapColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'xl'); // 1
	 * ```
	 * 
	 * @param   string  $class  A classname containing one or more Bootstrap col classes.
	 * @param   string  $size   A Bootstrap size. One of xs, sm, md, lg or xl.
	 * @return  integer         The number of Bootstrap columns
	 */
	public static function getBootstrapColumnCount($class, $size)
	{

		// Configure a static cache so repeated requests don't result in repeated processing.
		$key = "$class ~ $size";
		static $cache = [];

		// Check cache; return if found.
		if (array_key_exists($key, $cache)) {
			return $cache[$key];
		}

		// Define possible sizes and validate the specified size.
		$sizes = ['xl', 'lg', 'md', 'sm', 'xs'];
		if (!in_array($size, $sizes)) {
			throw new Exception('Invalid size: ' . $size);
		}

		// Define default.
		$count = 12;

		// Loop through the possible sizes starting at the specified size and getting smaller, checking to see if a class exists for it.
		// This works because Bootstrap applies 'sm' to the bigger sizes ('md' and 'lg') etc.
		for ($i = array_search($size, $sizes); $i < count($sizes); $i++) {
			// col-(\d+) accounts for the way the xs col is specified in bootstrap 4 
			if (preg_match("/\bcol-{$sizes[$i]}-(\d+)/", $class, $m) || preg_match("/\bcol-(\d+)/", $class, $m)) {
				$count = (int)$m[1];
				break;
			}
		}

		// Updated cache and return.
		$cache[$key] = $count;
		return $count;
	}


	/**
	 * Returns the number of content columns that would be displayed for the specified size.
	 *
	 * A content column is the number of visible columns that content appears to be in.
	 * For example, full width content is 1 column.
	 *
	 * For example:
	 * ```php
	 * KarlFrameBootstrap::getContentColumnCount('col-sm-6 col-md-4', 'xs'); // 1
	 * KarlFrameBootstrap::getContentColumnCount('col-sm-6 col-md-4', 'sm'); // 2
	 * KarlFrameBootstrap::getContentColumnCount('col-sm-6 col-md-4', 'md'); // 3
	 * KarlFrameBootstrap::getContentColumnCount('col-sm-6 col-md-4', 'lg'); // 3
	 * KarlFrameBootstrap::getContentColumnCount('col-sm-6 col-md-4', 'xl'); // 3
	 * 
	 * KarlFrameBootstrap::getContentColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'xs'); // 2
	 * KarlFrameBootstrap::getContentColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'sm'); // 3
	 * KarlFrameBootstrap::getContentColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'md'); // 4
	 * KarlFrameBootstrap::getContentColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'lg'); // 6
	 * KarlFrameBootstrap::getContentColumnCount('col-xs-6 col-sm-4 col-md-3 col-lg-2 col-xl-1', 'xl'); // 12
	 * ```
	 * 
	 * @param   string  $class  A classname containing one or more Bootstrap col-* classes.
	 * @param   string  $size   A Bootstrap size. One of xs, sm, md, lg or xl.
	 * @return  integer         The number of content columns
	 */
	public static function getContentColumnCount($class, $size)
	{
		return 12 / static::getBootstrapColumnCount($class, $size);
	}


	/**
	 * Automatically output clearfix DIVs where appropriate when looping through items to be displayed in a grid.
	 *
	 * In most cases there are only 2 steps to using this in an override:
	 * 
	 * 1. Load this class in your override (the reset is optional but recommended): `JLoader::register('KarlFrameBootstrap', 'templates/karlframe/KarlFrameBootstrap.php'); KarlFrameBootstrap::resetClearfixIndex();`
	 * 2. Call this function after displaying each item: `KarlFrameBootstrap::autoClearfix($classes);`
	 * 
	 * If the above doesn't work or if you have multiple nested loops through items that need clearfix DIVs, you should specify $index manually:
	 *
	 * 1. Load this class in your override: `JLoader::register('KarlFrameBootstrap', 'templates/karlframe/KarlFrameBootstrap.php');`
	 * 2. Initialize a counter before you loop through items: `$gridCount = 0;`
	 * 3. Call this function after displaying each item, passing it the grid classes being used: `KarlFrameBootstrap::autoClearfix($classes, ++$gridCount);`
	 *
	 * @param   string           $class  The classnames that determine the grid layout, eg "col-sm-6 col-md-4".
	 * @param   integer|boolean  $index  The index of the current item. Should start at 1 for the first item, and increment for each item in the grid. If set to true, KarlFrameBootstrap will count for you (which is safe as long as you're not calling this function in nested loops).
	 * @return  string[]                 A list of sizes for which the clearfix DIV was output. An empty array if the DIV was not output.
	 */
	public static function autoClearfix($class, $index = true)
	{

		// Automatically determined $index if it wasn't set.
		if ($index === true) {
			$index = ++static::$clearfixIndex;
		}

		// Determine the boostrap version in use, so we can use the correct display property class names
		$bootstrapVersion = static::getKarlFrameBootstrapVersion();

		$visible = [];

		// Determine visible-*-block for Bootstrap 3 OR let flexbox handle "clearfixing" on Bootstrap 4
		if ($bootstrapVersion === '4') {
			return $visible;
		} else {
			foreach (['xl', 'lg', 'md', 'sm', 'xs'] as $size) {
				$count = static::getContentColumnCount($class, $size);

				if ($count > 1 && $index % $count === 0) {
					$visible[$size] = sprintf('visible-%s-block', $size);
				}
			}

			// Output the DIV (if appropriate) and return.
			if (count($visible) > 0) {
				printf('<div class="clearfix %s" data-debug="%s / %s"></div>', join(' ', array_values($visible)), $class, $index);
			}
			return array_keys($visible);
		}
	}


	/**
	 * Sets or resets the variable autoClearfix uses to track the number of items that have been shown.
	 * @param   integer  $index  The new value.
	 */
	public static function resetClearfixIndex($index = 0)
	{
		static::$clearfixIndex = $index;
	}


	/**
	 * Gets and returns the bootstrap version selected in KarlFrame's options
	 * 
	 * @return string  The bootstrap version number
	 */
	public static function getKarlFrameBootstrapVersion()
	{
		return JFactory::getApplication()->getTemplate(true)->params->get('bootstrap-version', '3');
	}

	/**
	 * Automatically generates the column classes based on the sizes passed in for the boostrap version specified in options 
	 * OR uses the default values to generate the column classes for the grid
	 * 
	 * By default the column classes applied for Bootstrap versions are:
	 * Bootstrap 3 = col-xs-12 col-sm-6 col-md-4 col-lg-4 
	 * Bootstrap 4 = col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4
	 * 
	 * To override the defaults pass in the column sizes when calling the function
	 * e.g KarlFrameBootstrap::getBootstrapColumnClasses('6','6','4','3','3') outputs:
	 * Bootstrap 3 = col-xs-6 col-sm-4 col-md-3 col-lg-3 
	 * Bootstrap 4 = col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3
	 * 
	 * @param string  $xs  Column size for Extra Small devices
	 * @param string  $sm  Column size for Small Devices
	 * @param string  $md  Column size for Medium Devices
	 * @param string  $lg  Column size for Large Devices
	 * @param string  $xl  Column size for Extra Large Devices
	 * @return string      A string containing the column classes supported by the bootstrap version selected in KarlFrame
	 */
	public static function getBootstrapColumnClasses($xs = '12', $sm = '12', $md = '6', $lg = '4', $xl = '4')
	{

		$bootstrapVersion = static::getKarlFrameBootstrapVersion();

		$colSizes = [];
		$classes = [];

		if ($bootstrapVersion === '4') {
			$colSizes = [
				'xs' => $xs,
				'sm' => $sm,
				'md' => $md,
				'lg' => $lg,
				'xl' => $xl,
			];
		}

		// For Bootstrap 3 the sm = md, md = lg, lg = xl mapping below is done so that the columns 
		// display the same as bootstrap 4 and are not affected by the grid class name changes in bootstrap 4
		if ($bootstrapVersion === '3') {
			$colSizes = [
				'xs' => $xs,
				'sm' => $md,
				'md' => $lg,
				'lg' => $xl,
			];
		}

		foreach ($colSizes as $key => $size) {
			if ($bootstrapVersion === '4') {
				// Use col-* for xs and col-*-* for all other screen sizes
				$classes[] = $key != 'xs' ?  "col-" . $key . "-" . $size : "col-" . $size;
			} else {
				$classes[] = "col-" . $key . "-" . $size;
			}
		}

		return join(' ', $classes);
	}
}
