module.exports = function (grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			scripts: {
				options: {
					sourceMap: true,
				},
				files: {
					'../../source/scripts/karlframe.min.js': [
						'../../source/scripts/source/karlframe.orig.js',
					],
				},
			},
		},

		less: {
			options: {
				compress: true,
				sourceMap: true,
				sourceMapBasepath: '../../source/styles/',
				plugins: [
					new (require('less-plugin-autoprefix'))({ browsers: ['> 1%', 'Last 2 versions', 'Firefox ESR', 'Explorer 8 - 10'] }),
				],
			},
			style: {
				options: {
					sourceMapURL: 'style.min.css.map',
				},
				files: {
					'../../source/styles/style.min.css': '../../source/styles/source/style.less',
				},
			},
			menu: {
				options: {
					sourceMapURL: 'menu.min.css.map',
				},
				files: {
					'../../source/styles/menu.min.css': '../../source/styles/source/menu.less',
				},
			},
			editor: {
				options: {
					sourceMapURL: 'editor.min.css.map',
				},
				files: {
					'../../source/styles/editor.min.css': '../../source/styles/source/editor.less',
				},
			},
			editor_only: {
				options: {
					sourceMapURL: 'editor-only.min.css.map',
				},
				files: {
					'../../source/styles/editor-only.min.css': '../../source/styles/source/editor-only.less',
				},
			},
		},

		watch: {
			js: {
				files: ['../../source/scripts/source/*.orig.js'],
				tasks: ['uglify'],
			},
			minjs: {
				files: ['../../source/scripts/*.min.js'],
				options: {
					livereload: true,
				},
			},
			less: {
				files: ['../../source/styles/source/*.less'],
				tasks: ['less'],
			},
			css: {
				files: ['../../source/styles/*.min.css'],
				options: {
					livereload: true,
				},
			},
		},

	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['uglify', 'less', 'watch']);

};